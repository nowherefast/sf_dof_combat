import React from 'react';
import './App.css';

import Ship from './components/Ship'
import VehicleCombat from './components/VehicleCombat'

/*REACT Bootstrap Stuff*/
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
// react-router
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";


function App() {
  return (
    <BrowserRouter>
      <HomeMenu />
    </BrowserRouter>
  );
}
const HomeMenu = () => (
  <div>
    <BrowserRouter>
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="/">Starfinder Combat Tool</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/ship">Ship</Nav.Link>
          <Nav.Link href="/vehicle">Vehicle</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>

    {/*
      A <Switch> looks through all its children <Route>
      elements and renders the first one whose path
      matches the current URL. Use a <Switch> any time
      you have multiple routes, but you want only one
      of them to render at a time
    */}
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/ship">
        <Ship />
      </Route>
      <Route path="/vehicle">
        <VehicleCombat />
      </Route>
    </Switch>
    </BrowserRouter>
  </div>
);

const Home = () => (
  <div>
    <h2>Starfinder Combat Home Page</h2>
  </div>
);

export default App;
/**    <Container>
      <Ship></Ship>
    </Container> */
