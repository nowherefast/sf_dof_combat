import React from 'react';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import ProgressBar from 'react-bootstrap/ProgressBar';
import InputGroup from 'react-bootstrap/InputGroup';
import Table from 'react-bootstrap/Table';

import '../../../css/Ship.css'
import PopOutIcon from '../../../img/PopOut.png'

class HullForm extends React.Component {
    constructor(props){
       super(props);
    }

    render() {
        return(
            <div>
                <ProgressBar now={this.props.ship_active.hull.hp_percentage}/>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="HullpointsId" size="lg"><strong>Hull Points [Max={this.props.ship_active.hull.hp_max}]</strong></InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control
                        name="hp_cur"
                        placeholder="Current Hull Points"
                        aria-label="Hullpoints"
                        aria-describedby="HullpointsId"
                        size="lg"
                        value={this.props.ship_active.hull.hp_cur}
                        onChange={this.props.onShipHPChange}
                    />
                </InputGroup>
                <Table>
                    <tbody>
                    <tr>
                        <td><strong>AC: </strong> {this.props.ship_active.hull.ac_fill}</td>
                        <td><strong>TL: </strong> {this.props.ship_active.hull.tl_fill}</td>
                    </tr>
                    <tr>
                        <td><strong>DT: </strong> {this.props.ship_active.hull.dt_fill}</td>
                        <td><strong>CT: </strong> {this.props.ship_active.hull.ct_fill}</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default HullForm;