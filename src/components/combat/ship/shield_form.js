import React from 'react';
import Form from 'react-bootstrap/Form';

import '../../../css/Ship.css'

class ShieldForm extends React.Component {
    constructor(props){
       super(props);
    }
    /**
     * <strong>Forward</strong> [Max={this.state.ship_data[this.state.active_ship].shields.forward_shields_max}] 
     * onChange={(e) => this.handleShieldChange(e)} 
     * value={this.state.ship_data[active_ship].shields['forward_shields_cur']} 
     */
    render() {
        return(
            <Form>
                <Form.Label>
                    <h2 className="word_fanciness">Shields</h2>
                </Form.Label>
                <Form.Group controlId="fwdShieldArcGrp">
                    <Form.Label><strong>Forward</strong> [Max={this.props.ship_active.shields.forward_shields_max}] </Form.Label>
                    <Form.Control name="Forward" onChange={this.props.onShieldChange}
                        value={this.props.ship_active.shields['forward_shields_cur']} 
                        size="lg" type="input" placeholder="Fwd" />
                </Form.Group>
                <Form.Group controlId="portShieldArcGrp">
                    <Form.Label><strong>Port</strong> [Max={this.props.ship_active.shields.port_shields_max}]</Form.Label>
                    <Form.Control name="Port" onChange={this.props.onShieldChange}
                        value={this.props.ship_active.shields['port_shields_cur']}  
                        size="lg" type="input" placeholder="Prt" />
                </Form.Group>
                <Form.Group controlId="starboardShieldArcGrp">
                    <Form.Label><strong>Starboard</strong> [Max={this.props.ship_active.shields.starboard_shields_max}]</Form.Label>
                    <Form.Control name="Starboard" onChange={this.props.onShieldChange}
                        value={this.props.ship_active.shields['starboard_shields_cur']}   
                        size="lg" type="input" placeholder="Sbd" />
                </Form.Group>
                <Form.Group controlId="aftShieldArcGrp">
                    <Form.Label><strong>Aft</strong>  [Max={this.props.ship_active.shields.aft_shields_max}]</Form.Label>
                    <Form.Control name="Aft" onChange={this.props.onShieldChange}
                        value={this.props.ship_active.shields['aft_shields_cur']}
                        size="lg" type="input" placeholder="Aft" />
                </Form.Group>
            </Form>
        );
    }
}

export default ShieldForm;