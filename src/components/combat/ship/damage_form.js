import React from 'react';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';

import '../../../css/Ship.css'
import PopOutIcon from '../../../img/PopOut.png'

class DamageForm extends React.Component {
    constructor(props){
       super(props);
    }
    /**
onClick={() => this.showDamageReport()}
onChange={(e) => this.handleDamageChange(e)}
value={this.state.ship_data[this.state.active_ship].damage.life_support}
className={this.state.ship_data[this.state.active_ship].damage.life_support + '_color'}>
     */
    render() {
        return(
            <Form>
                <Form.Label>
                    <h2 className="word_fanciness">Dmg <Image onClick={this.props.onShowDamageReport} src={PopOutIcon} className='icon_size'/></h2> 
                </Form.Label>
                <Form.Group controlId="DmgLifeSupportGrpID">
                    <Form.Label><strong>Life:</strong></Form.Label>
                    <Form.Control as="select" 
                        name="life_support"
                        onChange={this.props.onDamageChange}
                        value={this.props.ship_active.damage.life_support}
                        className={this.props.ship_active.damage.life_support + '_color'}>
                        <option value="N">N</option>
                        <option value="G">G</option>
                        <option value="M">M</option>
                        <option value="W">W</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="DmgSensorsGrpID">
                    <Form.Label><strong>Sensors:</strong></Form.Label>
                    <Form.Control as="select" 
                        name="sensors"
                        onChange={this.props.onDamageChange}
                        value={this.props.ship_active.damage.sensors}
                        className={this.props.ship_active.damage.sensors + '_color'}>
                        <option value="N">N</option>
                        <option value="G">G</option>
                        <option value="M">M</option>
                        <option value="W">W</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="DmgWeaponsGrpID">
                    <Form.Label><strong>Weapons:</strong></Form.Label>
                    <Form.Control as="select" 
                        name="weapons_array"
                        onChange={this.props.onDamageChange}
                        value={this.props.ship_active.damage.weapons_array}
                        className={this.props.ship_active.damage.weapons_array + '_color'}>
                        <option value="N">N</option>
                        <option value="G">G</option>
                        <option value="M">M</option>
                        <option value="W">W</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="DmgEnginesGrpID">
                    <Form.Label><strong>Engines:</strong></Form.Label>
                    <Form.Control as="select"
                        name="engines"
                        onChange={this.props.onDamageChange}
                        value={this.props.ship_active.damage.engines}
                        className={this.props.ship_active.damage.engines + '_color'}>
                        <option value="N">N</option>
                        <option value="G">G</option>
                        <option value="M">M</option>
                        <option value="W">W</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="DmgPowerCoreGrpID">
                    <Form.Label><strong>Power:</strong></Form.Label>
                    <Form.Control as="select"
                        name="power_core"
                        onChange={this.props.onDamageChange}
                        value={this.props.ship_active.damage.power_core}
                        className={this.props.ship_active.damage.power_core + '_color'}>
                        <option value="N">N</option>
                        <option value="G">G</option>
                        <option value="M">M</option>
                        <option value="W">W</option>
                    </Form.Control>
                </Form.Group>
            </Form>
        );
    }
}

export default DamageForm;