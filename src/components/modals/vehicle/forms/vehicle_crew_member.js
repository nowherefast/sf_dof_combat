import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';

class VehicleCrewMemberModal extends React.Component {
    constructor(props){
       super(props);
    }
    render() {
        return(<Modal show={this.props.showModal} onHide={this.props.onCancelRequest} >
            <Modal.Header>
              <Modal.Title>Create Character</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={(e) => this.props.onSubmit(e)}>
                    <Form.Group controlId="characterIdGrp">
                        <Form.Label>ID</Form.Label>
                        <Form.Control type="input" placeholder="Enter Character ID" name="characterId" onChange={(e) => this.props.onCharacterChange('charId', e)}/>
                    </Form.Group>
                    <Form.Group controlId="characterNameGrp">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="input" placeholder="Enter Character Name" name="characterName" onChange={(e) => this.props.onCharacterChange('charName', e)}/>
                    </Form.Group>
                    <Form.Group controlId="playerNameGrp">
                        <Form.Label>Player Name</Form.Label>
                        <Form.Control type="input" placeholder="Enter Player Name" name="playerName" onChange={(e) => this.props.onCharacterChange('playerName', e)}/>
                    </Form.Group>
                    <Table>
                        <tbody>
                            <tr>
                                <td>
                                    <h5>Stamina</h5>                                 
                                </td>
                                <td>
                                    <Form.Group controlId="staminaMaxGrp">
                                        <Form.Control type="input" placeholder="Stam. (max)" name="staminaMax" onChange={(e) => this.props.onCharacterChange('staminaMax', e)}/>
                                    </Form.Group>                                    
                                </td>
                                <td>
                                    <h5>HP</h5>                                 
                                </td>
                                <td>
                                    <Form.Group controlId="hpMaxGrp">
                                        <Form.Control type="input" placeholder="HP (max)" name="hpMax" onChange={(e) => this.props.onCharacterChange('hpMax', e)}/>
                                    </Form.Group>                                     
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5>EAC</h5>                                 
                                </td>
                                <td>
                                    <Form.Group controlId="eacGrp">
                                        <Form.Control type="input" placeholder="EAC" name="eac" onChange={(e) => this.props.onCharacterChange('eac', e)}/>
                                    </Form.Group>                                    
                                </td>
                                <td>
                                    <h5>KAC</h5>                                 
                                </td>
                                <td>
                                    <Form.Group controlId="kacGrp">
                                        <Form.Control type="input" placeholder="KAC" name="kac" onChange={(e) => this.props.onCharacterChange('kac', e)}/>
                                    </Form.Group>                                     
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                    <Form.Group controlId="skillsGrp">
                        <Form.Label>Skills (enter total bonus)</Form.Label>
                        <Form.Control type="input" placeholder="acrobatics" name="acrobatics" onChange={(e) => this.props.onCharacterChange('skills', e)}/>
                        <Form.Control type="input" placeholder="athletics" name="athletics" onChange={(e) => this.props.onCharacterChange('skills', e)}/>
                        <Form.Control type="input" placeholder="piloting" name="piloting" onChange={(e) => this.props.onCharacterChange('skills', e)}/>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={this.props.onDownloadRequest}>Download</Button>
                <Button onClick ={this.props.onAddCharacter}>Add</Button>
                <Button onClick={this.props.onCancelRequest}>Cancel</Button>
            </Modal.Footer>
        </Modal>);
    }
}

/**
 *        <Button onClick={() => this.downloadCharacter()}>Download</Button>
          <Button onClick={() => this.addCharacter()}>Add</Button>
          <Button onClick={() => this.cancelCreateCharacterModal()}>Cancel</Button>
 */

export default VehicleCrewMemberModal;