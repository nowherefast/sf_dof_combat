import React from 'react';
import Table from 'react-bootstrap/Table';




const SpecialAbilitiesHidden = {
  'spec_ability_1':{
    'name':<td></td>,
    'desc':<td></td>
  },
  'spec_ability_2':{
    'name':<td></td>,
    'desc':<td></td>
  },
  'spec_ability_3':{
    'name':<td></td>,
    'desc':<td></td>
  },
  'spec_ability_4':{
    'name':<td></td>,
    'desc':<td></td>
  },
  'spec_ability_5':{
    'name':<td></td>,
    'desc':<td></td>
  },
  'spec_ability_6':{
    'name':<td></td>,
    'desc':<td></td>
  }
}

class SpecialAbilitiesList extends React.Component {
    constructor(props){
       super(props);
    }

    getSpecialAbilities(ability, exposed){
      let SpecialAbilitiesExposed = {
        'spec_ability_1':{
          'name':<td> <strong>{this.props.ship_active.special_abilities.spec_ability_name_1}</strong> </td>,
          'desc':<td> {this.props.ship_active.special_abilities.spec_ability_desc_1}</td>
        },
        'spec_ability_2':{
          'name':<td> <strong>{this.props.ship_active.special_abilities.spec_ability_name_2}</strong> </td>,
          'desc':<td> {this.props.ship_active.special_abilities.spec_ability_desc_2}</td>
        },
        'spec_ability_3':{
          'name':<td> <strong>{this.props.ship_active.special_abilities.spec_ability_name_3}</strong> </td>,
          'desc':<td> {this.props.ship_active.special_abilities.spec_ability_desc_3}</td>
        },
        'spec_ability_4':{
          'name':<td> <strong>{this.props.ship_active.special_abilities.spec_ability_name_4}</strong> </td>,
          'desc':<td> {this.props.ship_active.special_abilities.spec_ability_desc_4}</td>
        },
        'spec_ability_5':{
          'name':<td> <strong>{this.props.ship_active.special_abilities.spec_ability_name_5}</strong> </td>,
          'desc':<td> {this.props.ship_active.special_abilities.spec_ability_desc_5}</td>
        },
        'spec_ability_6':{
          'name':<td> <strong>{this.props.ship_active.special_abilities.spec_ability_name_6}</strong> </td>,
          'desc':<td> {this.props.ship_active.special_abilities.spec_ability_desc_6}</td>
        }
      }

      if(exposed){
        return SpecialAbilitiesExposed[ability];
      } else{
        return SpecialAbilitiesHidden[ability];
      }
    }


    render() {
        
        return(
          <Table>
          <tbody>
              <tr>
                 {this.getSpecialAbilities('spec_ability_1',this.props.ship_active.exposure.special_abilities['spec_ability_1']).name}
                 {this.getSpecialAbilities('spec_ability_1',this.props.ship_active.exposure.special_abilities['spec_ability_1']).desc}
              </tr>
                 {this.getSpecialAbilities('spec_ability_2',this.props.ship_active.exposure.special_abilities['spec_ability_2']).name}
                 {this.getSpecialAbilities('spec_ability_2',this.props.ship_active.exposure.special_abilities['spec_ability_2']).desc}  
              <tr>
                 {this.getSpecialAbilities('spec_ability_3',this.props.ship_active.exposure.special_abilities['spec_ability_3']).name}
                 {this.getSpecialAbilities('spec_ability_3',this.props.ship_active.exposure.special_abilities['spec_ability_3']).desc}  
              </tr>
              <tr>
                 {this.getSpecialAbilities('spec_ability_4',this.props.ship_active.exposure.special_abilities['spec_ability_4']).name}
                 {this.getSpecialAbilities('spec_ability_4',this.props.ship_active.exposure.special_abilities['spec_ability_4']).desc}       
              </tr>
              <tr>
                 {this.getSpecialAbilities('spec_ability_5',this.props.ship_active.exposure.special_abilities['spec_ability_5']).name}
                 {this.getSpecialAbilities('spec_ability_5',this.props.ship_active.exposure.special_abilities['spec_ability_5']).desc}     
              </tr>
              <tr>
                 {this.getSpecialAbilities('spec_ability_6',this.props.ship_active.exposure.special_abilities['spec_ability_6']).name}
                 {this.getSpecialAbilities('spec_ability_6',this.props.ship_active.exposure.special_abilities['spec_ability_6']).desc}    
              </tr>
          </tbody>
          </Table>
        );
    }
}

export default SpecialAbilitiesList;