import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';

class CharacterModal extends React.Component {
    constructor(props){
       super(props);
    }
    //showCreateCharacter
    //onChange={(e) => this.importCharacterData(e)}
    //onChange={(e) => this.handleCrewDetailChange(e)}
    //onChange={(e) => this.handleCrewChange('abilities', e)}
    render() {
        return(<Modal show={this.props.showModal} onHide={this.props.onCancelRequest} >
        <Modal.Header>
          <Modal.Title>Create Character</Modal.Title>
        </Modal.Header>
        <Modal.Body>
              <h4>Enter Character Details.</h4>
                  <p>
                  Enter details below
                  </p>
                  <p>
                  Or Import from File:
                  </p>
                  <Form.Control
                      accept='.json'
                      onChange={this.props.onImportCharacterData}
                      label="File"
                      type="file"
                      name="fileToImport" />
                  <hr />

              <Form>
                  <Form.Group controlId="characterIdGrp">
                      <Form.Label>ID</Form.Label>
                      <Form.Control type="input" placeholder="Enter Character Name" onChange={this.props.onCrewDetailChange} name="characterId"/>
                  </Form.Group>
                  <Form.Group controlId="characterNameGrp">
                      <Form.Label>Character Name</Form.Label>
                      <Form.Control type="input" placeholder="Enter Character Name" onChange={this.props.onCrewDetailChange} name="characterName"/>
                  </Form.Group>
                  <Form.Group controlId="playerNameGrp">
                      <Form.Label>Player Name</Form.Label>
                      <Form.Control type="input" placeholder="Enter Player Name" onChange={this.props.onCrewDetailChange} name="playerName"/>
                  </Form.Group>
                  <Form.Group controlId="babGrp">
                      <Form.Label>Base Attack</Form.Label>
                      <Form.Control type="input" placeholder="Enter BAB" onChange={this.props.onCrewDetailChange} name="baseAttack"/>
                  </Form.Group>
                  <Table>
                      <thead>
                          <tr>
                              <th>Str(mod)</th>
                              <th>Dex</th>
                              <th>Con</th>
                              <th>Int</th>
                              <th>Wis</th>
                              <th>Cha</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>
                                  <Form.Group controlId="strAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('abilities', e)} placeholder="#" name="strAbility_mod"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="dexAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('abilities', e)} placeholder="#" name="dexAbility_mod"/>
                                  </Form.Group>
                              </td> 
                              <td>
                                  <Form.Group controlId="conAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('abilities', e)} placeholder="#" name="conAbility_mod"/>
                                  </Form.Group>
                              </td> 
                              <td>
                                  <Form.Group controlId="intAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('abilities', e)} placeholder="#" name="intAbility_mod"/>
                                  </Form.Group>
                              </td> 
                              <td>
                                  <Form.Group controlId="wisAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('abilities', e)} placeholder="#" name="wisAbility_mod"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="chaAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('abilities', e)} placeholder="#" name="chaAbility_mod"/>
                                  </Form.Group>
                              </td>   
                          </tr>
                      </tbody>
                  </Table>
                  <Table>
                      <thead>
                          <tr>
                              <th>Skill Name</th>
                              <th>Ranks</th>
                              <th>Class Bonus</th>
                              <th>Ability Bonus</th>
                              <th>Misc</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>Bluff</td>
                              <td>
                                  <Form.Group controlId="bluffRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="bluff_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="bluffClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="bluff_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="bluffAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="bluff_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="bluffMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="bluff_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Computers</td>
                              <td>
                                  <Form.Group controlId="computersRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="computers_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="computersClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="computers_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="computersAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="computers_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="computersMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="computers_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Diplomacy</td>
                              <td>
                                  <Form.Group controlId="diplomacyRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="diplomacy_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="diplomacyClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="diplomacy_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="diplomacyAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="diplomacy_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="diplomacyMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="diplomacy_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Engineering</td>
                              <td>
                                  <Form.Group controlId="engineeringRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="engineering_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="engineeringClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="engineering_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="engineeringAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="engineering_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="engineeringMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="engineering_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Intimidate</td>
                              <td>
                                  <Form.Group controlId="intimidateRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="intimidate_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="intimidateClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="intimidate_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="intimidateAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="intimidate_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="intimidateMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="intimidate_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Life Science</td>
                              <td>
                                  <Form.Group controlId="lifeScienceRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="lifeScience_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="lifeScienceClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="lifeScience_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="lifeScienceAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="lifeScience_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="lifeScienceMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="lifeScience_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Physical Science</td>
                              <td>
                                  <Form.Group controlId="physicalScienceRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="physicalScience_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="physicalScienceClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="physicalScience_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="physicalScienceAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="physicalScience_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="physicalScienceMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="physicalScience_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                          <tr>
                              <td>Piloting</td>
                              <td>
                                  <Form.Group controlId="pilotingRanksGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="piloting_ranks"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="pilotingClassGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="piloting_class"/>
                                  </Form.Group>
                              </td>
                              <td>
                                  <Form.Group controlId="pilotingAbilityGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="piloting_ability"/>
                                  </Form.Group>                                    
                              </td>
                              <td>
                                  <Form.Group controlId="pilotingMiscGrp">
                                      <Form.Control type="input" onChange={(e) => this.props.onCrewChange('skills', e)} placeholder="#" name="piloting_misc"/>
                                  </Form.Group>                                     
                              </td>
                          </tr>
                      </tbody>
                  </Table>
              </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onDownloadRequest}>Download</Button>
          <Button onClick={this.props.onAddCharacterRequest}>Add</Button>
          <Button onClick={this.props.onCancelRequest}>Cancel</Button>
        </Modal.Footer>
    </Modal>);
    }
}

/**
 *        <Button onClick={() => this.downloadCharacter()}>Download</Button>
          <Button onClick={() => this.addCharacter()}>Add</Button>
          <Button onClick={() => this.cancelCreateCharacterModal()}>Cancel</Button>
 */

export default CharacterModal;