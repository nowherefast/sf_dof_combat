import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

class EncounterSaveModal extends React.Component {
    constructor(props){
       super(props);
    }
    //onHide={() => this.cancelSaveEncounterModal()}
    //onClick={() => this.downloadEncounter()}
    //onChange={(e) => this.handleEncounterNameChange(e)}
    //this.state.showSaveEncounter
    render() {
        return(
        <Modal show={this.props.showModal} onHide={this.props.onCancelRequest} >
          <Modal.Header>
            <Modal.Title>Save Encounter</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="EncounterGrp">
                <Form.Label> Save Encounter As... </Form.Label>
                <Form.Control name="encounter_name" onChange={(this.props.onEncounterNameChange)} 
                    size="lg" type="input" placeholder="Name" />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.onDownloadEncounter} variant="warning" size="lg" block>Save Encounter</Button>
            <Button onClick={this.props.onCancelRequest}>Cancel</Button>
          </Modal.Footer>
      </Modal>
        );
    }
}

export default EncounterSaveModal;