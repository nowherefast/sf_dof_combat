import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup'
import Table from 'react-bootstrap/Table'

class ShipCreateModal extends React.Component {
    constructor(props){
       super(props);
    }
    render() {
        //show={this.state.showCreateShip} onHide={() => this.cancelCreateShipModal()}
        //onHide={() => this.cancelCreateShipModal()}
        //onChange={(e) => this.importShipData(e)}
        //onChange={(e) => this.handleIdChange(e)}
        //onChange={(e) => this.handleDispositionChange(e)}
        //onChange={(e) => this.handleNewShipChange('stats', e)}
        /*
            <h5>Exposure</h5>
            {ExposureList}
        */
        return(
            <Modal show={this.props.showModal} onHide={this.props.onCancelRequest} >
                <Modal.Header closeButton>
                    <Modal.Title>Create Ship</Modal.Title>
                </Modal.Header>
            <Modal.Body>
            <h4>Enter Ship Details.</h4>
            <p>
            Enter details below
            </p>
            <p>
            Or Import from File:
            </p>
            <Form.Control
                accept='.json'
                onChange={this.props.onImport}
                label="File"
                type="file"
                name="fileToImport" />
            <hr />
            <h5>Ship ID</h5>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="ship_id"><strong>ID</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Ship ID (number)"
                    aria-label="ShipID"
                    name="ship_id"
                    onChange={this.props.onIdChange}
                    aria-describedby="ship_id"
                />
            </InputGroup>

            <h5>Disposition</h5>
            <Form.Group controlId="shipDispositionSelect">
                <Form.Label>Disposition</Form.Label>
                <Form.Control as="select" name='disposition' onChange={this.props.onDispositionChange}>
                    <option value='success'>PC</option>
                    <option value='danger'>Enemy</option>
                    <option value='primary'>NPC</option>
                </Form.Control>
            </Form.Group>

            <h5>Exposure</h5>
            {this.props.exposure}

            <h5>Details</h5>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="ship_tier_id"><strong>Ship Tier</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Tier Level"
                    aria-label="Name"
                    name="ship_tier"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="ship_tier_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="ship_name_id"><strong>Ship Name</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Name"
                    aria-label="Name"
                    name="ship_name"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="ship_name_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="classification_id"><strong>Classification</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Classification"
                    aria-label="Classification"
                    name="classification"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="classification_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="make_and_model_id"><strong>Make & Model</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Make and Model"
                    aria-label="make_and_model"
                    name="make_and_model"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="make_and_model_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="crew_compliment_id"><strong>Crew Compliment</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Crew Compliment"
                    aria-label="crew_compliment"
                    name="crew_compliment"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="crew_compliment_id"
                />
            </InputGroup>


            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="drift_engine_id"><strong>Drift Engine</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Drift Engine"
                    aria-label="drift_engine"
                    name="drift_engine"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="drift_engine_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="thrusters_id"><strong>Thrusters</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Thrusters"
                    aria-label="thrusters"
                    name="thrusters"
                    onChange={(e) => this.props.onNewShipChange('systems', e)}
                    aria-describedby="thrusters_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="armor_id"><strong>Armor</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Armor"
                    aria-label="armor"
                    name="armor"
                    onChange={(e) => this.props.onNewShipChange('systems', e)}
                    aria-describedby="armor_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="computer_id"><strong>Computer</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Computer"
                    aria-label="computer"
                    name="computer"
                    onChange={(e) => this.props.onNewShipChange('systems', e)}
                    aria-describedby="computer_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="sensors_id"><strong>Sensors</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Sensors"
                    aria-label="sensors"
                    name="sensors"
                    onChange={(e) => this.props.onNewShipChange('systems', e)}
                    aria-describedby="sensors_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="defensiveCounterMeasuresId"><strong>Defensive Countermeasures</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Defensive Countermeasures"
                    aria-label="DefensiveCountermeasures"
                    name="defensive_countermeasures"
                    onChange={(e) => this.props.onNewShipChange('systems', e)}
                    aria-describedby="defensiveCounterMeasuresId"
                />
            </InputGroup>
            {/**
                        'thrusters':'',
            'armor':'',
            'computer':'',
            'sensors':''
             */}




            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="power_core_id"><strong>Power Core</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Power Core"
                    aria-label="power_core"
                    name="power_core"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="power_core_id"
                />
            </InputGroup>



            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="modifiers_id"><strong>Modifiers</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Modifiers"
                    aria-label="modifiers"
                    name="modifiers"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="modifiers_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="expansion_bays_id"><strong>Expansion Bays</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Expansion Bays"
                    aria-label="ExpansionBays"
                    name="expansion_bays"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="expansion_bays_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="misc_systems_id"><strong>Misc Systems</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Misc Systems"
                    aria-label="MiscSystems"
                    name="misc_systems"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="misc_systems_id"
                />
            </InputGroup>

            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="cargo_id"><strong>Cargo</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Cargo"
                    aria-label="Cargo"
                    name="cargo"
                    onChange={(e) => this.props.onNewShipChange('stats', e)}
                    aria-describedby="cargo_id"
                />
            </InputGroup>

            <Table>
                <tbody>
                <tr>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="maneuverability_id"><strong>Maneuv.</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Maneuv."
                                aria-label="maneuverability"
                                name="maneuverability"
                                onChange={(e) => this.props.onNewShipChange('stats', e)}
                                aria-describedby="maneuverability_id"
                            />
                        </InputGroup>
                    </td>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="speed_id"><strong>Speed</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Speed"
                                aria-label="Speed"
                                name="speed"
                                onChange={(e) => this.props.onNewShipChange('stats', e)}
                                aria-describedby="speed_id"
                            />
                        </InputGroup>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="drift_rating_id"><strong>Drift Rate</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Rate"
                                aria-label="drift_rating"
                                name="drift_rating"
                                onChange={(e) => this.props.onNewShipChange('stats', e)}
                                aria-describedby="drift_rating_id"
                            />
                        </InputGroup>                        
                    </td>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="size_id"><strong>Size</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Size"
                                aria-label="size"
                                name="size"
                                onChange={(e) => this.props.onNewShipChange('stats', e)}
                                aria-describedby="size_id"
                            />
                        </InputGroup>                        
                    </td>
                </tr>
                </tbody>
            </Table>
            <p> Defenses </p>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="hp_max_id"><strong>Hull Points</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="HP"
                    aria-label="HullPoints"
                    onChange={(e) => this.props.onNewShipChange('hull', e)}
                    name="hp_max"
                    aria-describedby="hp_max_id"
                />
            </InputGroup>  


            <Table>
                <tbody>
                <tr>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="ac_fill_id"><strong>AC FILL</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="AC FILL"
                                aria-label="ac_fill"
                                onChange={(e) => this.props.onNewShipChange('hull', e)}
                                name="ac_fill"
                                aria-describedby="ac_fill_id"
                            />
                        </InputGroup>                        
                    </td>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="tl_fill_id"><strong>TL FILL</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="TL FILL"
                                aria-label="TLFILL"
                                onChange={(e) => this.props.onNewShipChange('hull', e)}
                                name="tl_fill"
                                aria-describedby="tl_fill_id"
                            />
                        </InputGroup>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="dt_fill_id"><strong>DT FILL</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="DT FILL"
                                aria-label="dt"
                                onChange={(e) => this.props.onNewShipChange('hull', e)}
                                name="dt_fill"
                                aria-describedby="dt_fill_id"
                            />
                        </InputGroup>                        
                    </td>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="ct_fill_id"><strong>CT FILL</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="CT FILL"
                                aria-label="ct_fill"
                                onChange={(e) => this.props.onNewShipChange('hull', e)}
                                name="ct_fill"
                                aria-describedby="ct_fill_id"
                            />
                        </InputGroup>                        
                    </td>
                </tr>
                </tbody>
            </Table>
            <p> Shields </p>
            <Table>
                <tbody>
                <tr>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="shield_fwd_id"><strong>Fwd</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Forward"
                                aria-label="shield_fwd"
                                name="forward_shields_max"
                                onChange={(e) => this.props.onNewShipChange('shields', e)}
                                aria-describedby="shield_fwd_id"
                            />
                        </InputGroup>                        
                    </td>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="shield_aft_id"><strong>Aft</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Aft"
                                aria-label="aft"
                                name="aft_shields_max"
                                onChange={(e) => this.props.onNewShipChange('shields', e)}
                                aria-describedby="shield_aft_id"
                            />
                        </InputGroup>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="shield_port_id"><strong>Port</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Port"
                                aria-label="shield_port"
                                name="port_shields_max"
                                onChange={(e) => this.props.onNewShipChange('shields', e)}
                                aria-describedby="shield_port_id"
                            />
                        </InputGroup>                        
                    </td>
                    <td>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="shield_starboard_id"><strong>Star</strong></InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Starboard"
                                aria-label="starboard"
                                name="starboard_shields_max"
                                onChange={(e) => this.props.onNewShipChange('shields', e)}
                                aria-describedby="shield_starboard_id"
                            />
                        </InputGroup>                        
                    </td>
                </tr>
                </tbody>                      
            </Table>
            <p> Weapons </p>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="fwd_weapons_id"><strong>Forward</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Forward Weapons (comma separated)"
                    aria-label="fwd_weapons"
                    name="forward_weapons"
                    onChange={(e) => this.props.onNewShipChange('weapons', e)}
                    aria-describedby="fwd_weapons_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="port_weapons_id"><strong>Port</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Port Weapons (comma separated)"
                    aria-label="port_weapons"
                    name="port_weapons"
                    onChange={(e) => this.props.onNewShipChange('weapons', e)}
                    aria-describedby="port_weapons_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="starboard_weapons_id"><strong>Starboard</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Starboard Weapons (comma separated)"
                    aria-label="starboard_weapons"
                    name="starboard_weapons"
                    onChange={(e) => this.props.onNewShipChange('weapons', e)}
                    aria-describedby="starboard_weapons_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="aft_weapons_id"><strong>Aft</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Aft Weapons (comma separated)"
                    aria-label="aft_weapons"
                    name="aft_weapons"
                    onChange={(e) => this.props.onNewShipChange('weapons', e)}
                    aria-describedby="aft_weapons_id"
                />
            </InputGroup>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text id="turret_weapons_id"><strong>Turrets</strong></InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                    placeholder="Turret Weapons (comma separated)"
                    aria-label="turret_weapons"
                    name="turret_weapons"
                    onChange={(e) => this.props.onNewShipChange('weapons', e)}
                    aria-describedby="turret_weapons_id"
                />
            </InputGroup>
            <p> Special Abilities </p>
            <Table>
                <tbody>
                    <tr>
                        <td>
                        <Form.Control
                            placeholder="Name"
                            aria-label="spec_ability_name_1"
                            name="spec_ability_name_1"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_name_1_id"
                        />
                        </td>
                        <td>
                        <Form.Control
                            placeholder="Description"
                            aria-label="spec_ability_desc_1"
                            name="spec_ability_desc_1"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_desc_1_id"
                        />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <Form.Control
                            placeholder="Name"
                            aria-label="spec_ability_name_2"
                            name="spec_ability_name_2"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_name_2_id"
                        />
                        </td>
                        <td>
                        <Form.Control
                            placeholder="Description"
                            aria-label="spec_ability_desc_2"
                            name="spec_ability_desc_2"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_desc_2_id"
                        />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <Form.Control
                            placeholder="Name"
                            aria-label="spec_ability_name_3"
                            name="spec_ability_name_3"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_name_3_id"
                        />
                        </td>
                        <td>
                        <Form.Control
                            placeholder="Description"
                            aria-label="spec_ability_desc_3"
                            name="spec_ability_desc_3"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_desc_3_id"
                        />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <Form.Control
                            placeholder="Name"
                            aria-label="spec_ability_name_4"
                            name="spec_ability_name_4"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_name_4_id"
                        />
                        </td>
                        <td>
                        <Form.Control
                            placeholder="Description"
                            aria-label="spec_ability_desc_4"
                            name="spec_ability_desc_4"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_desc_4_id"
                        />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <Form.Control
                            placeholder="Name"
                            aria-label="spec_ability_name_5"
                            name="spec_ability_name_5"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_name_5_id"
                        />
                        </td>
                        <td>
                        <Form.Control
                            placeholder="Description"
                            aria-label="spec_ability_desc_5"
                            name="spec_ability_desc_5"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_desc_5_id"
                        />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <Form.Control
                            placeholder="Name"
                            aria-label="spec_ability_name_6"
                            name="spec_ability_name_6"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_name_6_id"
                        />
                        </td>
                        <td>
                        <Form.Control
                            placeholder="Description"
                            aria-label="spec_ability_desc_6"
                            name="spec_ability_desc_6"
                            onChange={(e) => this.props.onNewShipChange('special_abilities', e)}
                            aria-describedby="spec_ability_desc_6_id"
                        />
                        </td>
                    </tr>
                </tbody>
            </Table>
            
        </Modal.Body>
        <Modal.Footer>
            <Button onClick={this.props.onDownload}>Download</Button>
            <Button onClick={this.props.onAddShip}>Add</Button>
            <Button onClick={this.props.onCancelRequest}>Cancel</Button>
        </Modal.Footer>
      </Modal>
        );
    }
}

/**
 *          <Button onClick={() => this.downloadShipData()}>Download</Button>
            <Button onClick={(e) => this.addShip(e)}>Add</Button>
            <Button onClick={() => this.cancelCreateShipModal()}>Cancel</Button>
 */

export default ShipCreateModal;