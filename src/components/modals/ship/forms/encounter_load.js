import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

class EncounterLoadModal extends React.Component {
    constructor(props){
       super(props);
    }
    render() {
        return(
        <Modal show={this.props.showModal} onHide={this.props.onCancelRequest} >
          <Modal.Header>
            <Modal.Title>Loading Encounter...</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="EncounterGrp">
                <Form.Label> Load Encounter </Form.Label>
                <Form.Control
                accept='.json'
                onChange={this.props.onImportEncounter}
                label="File"
                type="file"
                name="fileToImport" />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.onCancelRequest}>Cancel</Button>
          </Modal.Footer>
        </Modal>
        );
    }
}

export default EncounterLoadModal;