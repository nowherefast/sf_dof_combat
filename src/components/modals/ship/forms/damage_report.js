import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';


const dmgNominalTxt = "This system is working nominally.";
const dmgGlitchingTxt = "A glitching system isn’t operating at peak performance. Crew actions involving the system (except the hold it together and patch engineer actions; see page 323) take a –2 penalty.";
const dmgMalfunctioningTxt =  "A malfunctioning system is difficult to control. Crew actions " +
                            "involving the system (except the hold it together and patch " +
                            "engineer actions) take a –4 penalty. Also, crew members can’t take " +
                            "push actions (see page 322) using that system. If the power core " +
                            "is malfunctioning, all actions aboard the starship not involving the " +
                            "power core take a –2 penalty; this penalty stacks with penalties " +
                            "from critical damage conditions affecting other systems.";
const dmgWreckedTxt = "A wrecked system is minimally functional. Crew actions " +
                    "involving the system (except the hold it together and patch " +
                    "engineer actions and minor crew actions; see page 326) " +
                    "automatically fail. If the power core is wrecked, all crew actions " +
                    "aboard the starship not involving the power core take a –4 " +
                    "penalty; this penalty stacks with penalties from critical damage " +
                    "conditions affecting other systems.";
const dmgLifeSupportTxt = "Condition applies to all captain actions";
const dmgSensorsTxt = "Condition applies to all science officer actions";
const dmgWeaponsArrayTxt = "Randomly determine one arc containing weapons; condition applies to all gunner actions using weapons in that arc (a turret counts as being in all arcs)";
const dmgEnginesTxt = "Condition applies to all pilot actions";
const dmgPowerCoreTxt = "Condition applies to all engineer actions except hold it together and patch; a malfunctioning or wrecked power core affects other crew members’ actions (see Critical Damage Conditions)";

class DamageReportModal extends React.Component {
    constructor(props){
       super(props);
    }
    /**
     *   weapons_array': '',
        'sensors': '',
        'life_support': '',
        'engines': '',
        'power_core': ''

     */
    getWeaponsArrayReport(weapons_array){
        switch(weapons_array) {
            case 'N':
                return(<div>
                    <h3>Weapons Array:</h3>
                    <p>{dmgWeaponsArrayTxt}</p>
                    <hr/>
                    <p>{dmgNominalTxt}</p>
                </div>);
            case 'G':
                return(<div>
                    <h3>Weapons Array:</h3>
                    <p>{dmgWeaponsArrayTxt}</p>
                    <hr/>
                    <p>{dmgGlitchingTxt}</p>
                </div>);
            case 'M':
                return(<div>
                    <h3>Weapons Array:</h3>
                    <p>{dmgWeaponsArrayTxt}</p>
                    <hr/>
                    <p>{dmgMalfunctioningTxt}</p>
                </div>);
            case 'W':
                return(<div>
                    <h3>Weapons Array:</h3>
                    <p>{dmgWeaponsArrayTxt}</p>
                    <hr/>
                    <p>{dmgWreckedTxt}</p>
                </div>);
            default:
                break;
        }
    }
    getSensorsReport(sensors){
        switch(sensors) {
            case 'N':
                return(<div>
                    <h3>Sensors:</h3>
                    <p>{dmgSensorsTxt}</p>
                    <hr/>
                    <p>{dmgNominalTxt}</p>
                </div>);
            case 'G':
                return(<div>
                    <h3>Sensors:</h3>
                    <p>{dmgSensorsTxt}</p>
                    <hr/>
                    <p>{dmgGlitchingTxt}</p>
                </div>);
            case 'M':
                return(<div>
                    <h3>Sensors:</h3>
                    <p>{dmgSensorsTxt}</p>
                    <hr/>
                    <p>{dmgMalfunctioningTxt}</p>
                </div>);
            case 'W':
                return(<div>
                    <h3>Sensors:</h3>
                    <p>{dmgSensorsTxt}</p>
                    <hr/>
                    <p>{dmgWreckedTxt}</p>
                </div>);
            default:
                break;
        }
    }
    getLifeSupportReport(life_support){
        switch(life_support) {
            case 'N':
                return(<div>
                    <h3>Life Support:</h3>
                    <p>{dmgLifeSupportTxt}</p>
                    <hr/>
                    <p>{dmgNominalTxt}</p>
                </div>);
            case 'G':
                return(<div>
                    <h3>Life Support:</h3>
                    <p>{dmgLifeSupportTxt}</p>
                    <hr/>
                    <p>{dmgGlitchingTxt}</p>
                </div>);
            case 'M':
                return(<div>
                    <h3>Life Support:</h3>
                    <p>{dmgLifeSupportTxt}</p>
                    <hr/>
                    <p>{dmgMalfunctioningTxt}</p>
                </div>);
            case 'W':
                return(<div>
                    <h3>Life Support:</h3>
                    <p>{dmgLifeSupportTxt}</p>
                    <hr/>
                    <p>{dmgWreckedTxt}</p>
                </div>);
            default:
                break;
        }
    }
    getEngineReport(engine){
        switch(engine) {
            case 'N':
                return(<div>
                    <h3>Engine:</h3>
                    <p>{dmgEnginesTxt}</p>
                    <hr/>
                    <p>{dmgNominalTxt}</p>
                </div>);
            case 'G':
                return(<div>
                    <h3>Engine:</h3>
                    <p>{dmgEnginesTxt}</p>
                    <hr/>
                    <p>{dmgGlitchingTxt}</p>
                </div>);
            case 'M':
                return(<div>
                    <h3>Engine:</h3>
                    <p>{dmgEnginesTxt}</p>
                    <hr/>
                    <p>{dmgMalfunctioningTxt}</p>
                </div>);
            case 'W':
                return(<div>
                    <h3>Engine:</h3>
                    <p>{dmgEnginesTxt}</p>
                    <hr/>
                    <p>{dmgWreckedTxt}</p>
                </div>);
            default:
                break;
        }
    }
    getPowerCoreReport(power_core){
        switch(power_core) {
            case 'N':
                return(<div>
                    <h3>Power Core:</h3>
                    <p>{dmgPowerCoreTxt}</p>
                    <hr/>
                    <p>{dmgNominalTxt}</p>
                </div>);
            case 'G':
                return(<div>
                    <h3>Power Core:</h3>
                    <p>{dmgPowerCoreTxt}</p>
                    <hr/>
                    <p>{dmgGlitchingTxt}</p>
                </div>);
            case 'M':
                return(<div>
                    <h3>Power Core:</h3>
                    <p>{dmgPowerCoreTxt}</p>
                    <hr/>
                    <p>{dmgMalfunctioningTxt}</p>
                </div>);
            case 'W':
                return(<div>
                    <h3>Power Core:</h3>
                    <p>{dmgPowerCoreTxt}</p>
                    <hr/>
                    <p>{dmgWreckedTxt}</p>
                </div>);
            default:
                break;
        }
    }
    render() {
        return(
        <Modal show={this.props.showModal} size="lg" onHide={this.props.onCloseRequest}>
            <Modal.Header>
            <Modal.Title>Damage Report</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {this.getWeaponsArrayReport(this.props.ship_active.damage.weapons_array)}
                {this.getSensorsReport(this.props.ship_active.damage.sensors)}
                {this.getLifeSupportReport(this.props.ship_active.damage.life_support)}
                {this.getEngineReport(this.props.ship_active.damage.engine)}
                {this.getPowerCoreReport(this.props.ship_active.damage.power_core)}              
            </Modal.Body>
            <Modal.Footer>
            <Button onClick={this.props.onCloseRequest}>Close</Button>
            </Modal.Footer>
        </Modal>
        );
    }
}

export default DamageReportModal;