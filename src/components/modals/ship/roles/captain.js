import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse'

class CaptainModal extends React.Component {
    constructor(props){
       super(props);
       this.state = {
        showDemand: false,
        showEncourage: false,
        showTaunt: false,
        showOrders: false,
        showMovingSpeech: false
       }
    }

    toggleShowDemand(isDemandShown){
      //Just close modal for now
      let newState = this.state;
      newState.showDemand = isDemandShown;
      this.setState(newState);
    }
    toggleShowEncourage(isEncourageShown){
      //Just close modal for now
      let newState = this.state;
      newState.showEncourage = isEncourageShown;
      this.setState(newState);
    }
    toggleShowTaunt(isTauntShown){
      //Just close modal for now
      let newState = this.state;
      newState.showTaunt = isTauntShown;
      this.setState(newState);
    }
    toggleShowOrders(isOrderShown){
      //Just close modal for now
      let newState = this.state;
      newState.showOrders = isOrderShown;
      this.setState(newState);
    }
    toggleShowMovingSpeech(isMovingSpeechShown){
      //Just close modal for now
      let newState = this.state;
      newState.showMovingSpeech = isMovingSpeechShown;
      this.setState(newState);
    }

    render() {
        return(<Modal show={this.props.showModal} size="lg" onHide={this.props.onCloseRequest}>
        <Modal.Header>
          <Modal.Title>Captain Actions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Demand (Any Phase)&nbsp;&nbsp;
          <Button
            onClick={() => this.toggleShowDemand(!this.state.showDemand)}
            size="sm"
          >
            Details
          </Button>
          </h5>
          <hr/>
          <Collapse in={this.state.showDemand}>
            <div>
            <p>
            You can make a demand of a crew member to try to improve
            his performance. You grant a +4 bonus to one specific check by
            succeeding at an Intimidate check (DC = 15 + 2 × your starship’s
            tier). You must use this action before the associated check is rolled,
            and you can grant this bonus to an individual character only once
            per combat. Demand might result in negative consequences if
            used on NPCs, and you can’t make demands of yourself.
            </p>
            <p>
              <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
              Your demand echoes throughout the starship. You can
              attempt the demand crew action with the targeted character
              one additional time during the current starship combat.
            </p>
            </div>
          </Collapse>
          <h5>Encourage (Any Phase)&nbsp;&nbsp;
            <Button
              onClick={() => this.toggleShowEncourage(!this.state.showEncourage)}
              size="sm"
            >
              Details
            </Button>
          </h5>
          <hr/>
          <Collapse in={this.state.showEncourage}>
            <div>
              <p>
              You can encourage another member of the crew to give her a
              bonus to her action. This works like aid another (see page 133),
              granting a +2 bonus to the check required by a crew action if you
              succeed at a DC 10 check using the same skill. Alternatively, you
              can grant this same bonus by succeeding at a Diplomacy check
              (DC = 15 + your starship’s tier). You can’t encourage yourself.
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                You’re able to select just the right motivation
                for your crew, increasing the bonus you grant to your crew
                members’ actions to +4.
              </p>
            </div>
          </Collapse>
          <h5>Taunt (Any Phase, Push)&nbsp;&nbsp;
            <Button
              onClick={() => this.toggleShowTaunt(!this.state.showTaunt)}
              size="sm"
            >
              Details
            </Button>
          </h5>
          <hr/>
          <Collapse in={this.state.showTaunt}>
            <div>
              <p>
              You can use the communications system to broadcast a taunting
              message to the enemy vessel. You select an enemy vessel and
              a phase of combat (engineering, helm, or gunnery), and then
              attempt a Bluff or Intimidate check (DC = 15 + 2 × the enemy
              starship’s tier). If you are successful, each enemy character
              acting during the selected phase takes a –2 penalty to all checks
              for 1d4 rounds; the penalty increases to –4 if the enemy’s check
              is made as part of a push action. Once used against an enemy
              starship, regardless of the outcome, taunt can’t be used against
              that starship again during the same combat.
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                Your strong words push just the right buttons. The
                penalty your taunt action applies to an enemy ship continues
                through all three phases of combat instead of just one.
              </p>
            </div>
          </Collapse>
          <h5>Orders (Any Phase, Push)&nbsp;&nbsp;
            <Button
              onClick={() => this.toggleShowOrders(!this.state.showOrders)}
              size="sm"
            >
              Details
            </Button>
          </h5>
          <hr/>
          <Collapse in={this.state.showOrders}>
            <div>
              <p>
              At 6th level, you can grant an additional action to one member
              of the crew by spending 1 Resolve Point and succeeding at a
              difficult skill check at the beginning of the phase in which the
              crew member would normally act. The type of check depends on
              the role of the crew member targeted: a Computers check for a
              science officer, an Engineering check for an engineer, a gunnery
              check (see page 320) for a gunner, and a Piloting check for a
              pilot. The DC of this check is equal to 10 + 3 × your starship’s tier.
              If the check succeeds, the crew member can take two actions in
              her role this round (both when she would normally act), but she
              can’t take the same action twice. You can’t give yourself orders.
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                You orders flow so naturally that you can take one
              additional captain crew action this turn.
              </p>
            </div>
          </Collapse>            
          <h5>Moving Speech (Any Phase)&nbsp;&nbsp;
            <Button
              onClick={() => this.toggleShowMovingSpeech(!this.state.showMovingSpeech)}
              size="sm"
            >
              Details
            </Button>
          </h5>
          <hr/>
          <Collapse in={this.state.showMovingSpeech}>
            <div>
              <p>
              At 12th level, you can spend 1 Resolve Point and use your action
              to give a moving speech to the crew during one phase of combat
              with a successful Diplomacy check (DC = 15 + 2 × your starship’s
              tier). For the remainder of that phase, your allies can roll twice
              and take the better result when performing crew actions.
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                The crew is so motivated by your speech
                that they also gain a +2 bonus to all checks that phase, as if you
                had successfully used the encourage action to aid them.
              </p>
            </div>
          </Collapse>            
        </Modal.Body>
        <Modal.Footer>
          <Button name="captain" onClick={this.props.onCloseRequest}>Close</Button>
        </Modal.Footer>
    </Modal>);
    }
}

export default CaptainModal;