import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse';
import Image from 'react-bootstrap/Image';

import Stunts from '../../../../img/Stunts.PNG'


class PilotModal extends React.Component {
    constructor(props){
       super(props);
       this.state = {
         showFly: false,
         showManeuver: false,
         showStunt: false,
         showFullPower: false,
         showAudaciousGambit: false
       } 
    }

    toggleShowFly(isFlyShown){
      //Just close modal for now
      let newState = this.state;
      newState.showFly = isFlyShown;
      this.setState(newState);
    }
    toggleShowManeuver(isManeuverShown){
      //Just close modal for now
      let newState = this.state;
      newState.showManeuver = isManeuverShown;
      this.setState(newState);
    }
    toggleShowStunt(isStuntShown){
      //Just close modal for now
      let newState = this.state;
      newState.showStunt = isStuntShown;
      this.setState(newState);
    }
    toggleShowFullPower(isFullPowerShown){
      //Just close modal for now
      let newState = this.state;
      newState.showFullPower = isFullPowerShown;
      this.setState(newState);
    }
    toggleShowAudaciousGambit(isAudaciousGambitShown){
      //Just close modal for now
      let newState = this.state;
      newState.showAudaciousGambit = isAudaciousGambitShown;
      this.setState(newState);
    }

    render() {
        return(<Modal show={this.props.showModal} size="lg" onHide={this.props.onCloseRequest}>
        <Modal.Header>
          <Modal.Title>Pilot Actions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Fly (Helm Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowFly(!this.state.showFly)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showFly}>
            <p>
            You move your starship up to its speed and can make any turns
            allowed by its maneuverability. This doesn’t require a skill check. 
            </p>
          </Collapse>
          <h4>Maneuver (Helm Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowManeuver(!this.state.showManeuver)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showManeuver}>
            <div>
              <p>
              You move your starship up to its speed. You can also attempt
              a Piloting check (DC = 15 + 2 × your starship’s tier) to reduce
              your starship’s distance between turns by 1 (to a minimum of 0).
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                Fancy flying keeps you safe through incoming
                fire. You gain the effects of a successful evade stunt until the
                start of the next round.
              </p>
            </div>
          </Collapse>
          <h4>Stunt (Helm Phase, Push)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowStunt(!this.state.showStunt)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showStunt}>
            <div>
              <p>
              You can attempt any one of the stunts described on page 319.
              The DCs of the Piloting checks required and the results of
              success and failure are described in each stunt’s description.
              </p>
            
              <p>
                  <Image src={Stunts}/>
                  <ul>
                      <li>
                      <strong>Back Off -- </strong>
                      The starship moves up to half its speed in the direction of the aft
                      edge without changing facing. It can’t take any turns during this
                      movement. To perform this stunt, you must succeed at a Piloting
                      check (DC = 10 + 2 × your starship’s tier). On a failed check, your
                      starship moves backward only 1 hex. If you fail this check by 5 or
                      more, your starship does not move at all and takes a –4 penalty
                      to its AC and TL until the start of the next round.
                      <ul>
                        <li>
                          <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                          Your starship can move up to its full speed and
                          make turns as normal for its maneuverability rating.
                        </li>
                      </ul>
                      </li>
                      <li>
                      <strong>Barrel Roll -- </strong>
                      The starship moves up to half its speed and flips along its central
                      axis. For the next gunnery phase, the starship’s port shields and
                      weapons function as if they were in the starboard firing arc and
                      vice versa. The starship reverts to normal at the beginning of the
                      next round. To perform this stunt, your starship must be Large
                      or smaller and you must succeed at a Piloting check (DC = 10 + 2
                      × your starship’s tier). On a failed check, the starship moves half
                      its speed but doesn’t roll. If you fail by 5 or more, your starship
                      moves half its speed, doesn’t roll, and takes a –4 penalty to its AC
                      and TL until the start of the next round.
                      <ul>
                        <li>
                          <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                          The sudden roll makes it difficult to target your
                          starship. You also gain the effects of a successful evade stunt
                          until the start of the next round.
                        </li>
                      </ul>
                      </li>
                      <li>
                      <strong>Evade -- </strong>
                      The ship moves up to its speed and can turn as normal, but it
                      gains a +2 circumstance bonus to its AC and TL until the start
                      of the next round. To perform this stunt, you must succeed at a
                      Piloting check (DC = 10 + 2 × your starship’s tier). If you fail, the
                      starship moves as normal. If you fail the check by 5 or more, the
                      starship moves as normal, but it also takes a –2 penalty to its AC
                      and TL until the start of the next round.
                      <ul>
                        <li>
                          <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                          You successfully anticipate your enemy’s firing
                          patterns. The stunt’s circumstance bonus to your starship’s AC
                          and TL increases to +4.
                        </li>
                      </ul>
                      </li>
                      <li>
                      <strong>Flip and Burn -- </strong>
                      The ship moves forward up to half its speed (without turning)
                      and rotates 180 degrees to face the aft edge at the end of the
                      movement. To perform this stunt, you must succeed at a Piloting
                      check (DC = 15 + 2 × your ship’s tier). If you fail this check, your
                      starship moves forward half its speed but doesn’t rotate.
                      <ul>
                        <li>
                          <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                          With a flawlessly executed stunt, your starship
                          can move up to its full speed (without turning) and rotate 180
                          degrees to face the aft edge at the end of the movement.        
                        </li>
                      </ul>
                      </li>                
                      <li>
                      <strong>Flyby --</strong>
                      The ship moves as normal, but it can move through 1 hex
                      occupied by an enemy starship without provoking a free attack
                      (as described in Moving through Other Starships). During the
                      following gunnery phase, you can select one arc of your starship’s
                      weapons to fire at the enemy vessel as if the vessel were in close
                      range (treat the range as 1 hex), against any quadrant of the
                      enemy starship. To perform this stunt, you must succeed at a
                      Piloting check (DC = 20 + 2 × the tier of the enemy starship).
                      If you fail this check, your starship still moves as described
                      above, but you follow the normal rules for attacking (based on
                      320 STARSHIPS
                      your starship’s final position and distance), and the movement
                      provokes a free attack from that starship as normal.
                      <ul>
                        <li>
                          <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                          You line up the shot perfectly. The gunner gains a +2
                          circumstance bonus to the gunnery check affected by this stunt.
                        </li>
                      </ul>
                      </li>
                      <li>
                      <strong>Slide --</strong>
                      The starship moves up to its speed in the direction of either the
                      forward-port or forward-starboard edge without changing its
                      facing. To perform this stunt, you must succeed at a Piloting
                      check (DC = 10 + 2 × your ship’s tier). If you fail this check, the
                      ship moves forward up to half its speed and can’t make any turns.
                      <ul>
                        <li>
                          <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                          You slide and rotate your starship simultaneously. At
                          the end of the stunt’s movement, your starship can turn once.
                        </li>
                      </ul>    
                      </li>
                      <li>
                      <strong>Turn in Place --</strong>
                      The ship does not move but instead can turn to face any
                      direction. If the ship has a maneuverability of clumsy, it takes
                      a –4 penalty to its AC and TL until the start of the next round.
                      If it has a maneuverability of poor, it instead takes a –2 penalty
                      to its AC and TL until the start of the next round. Ships with a
                      maneuverability of average or better do not take a penalty. This
                      stunt doesn’t require a skill check.  
                      </li>

                  </ul>
              </p>
            </div>
          </Collapse>

          <h4>Full Power (Helm Phase, Push)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowFullPower(!this.state.showFullPower)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showFullPower}>
            <div>
              <p>
              If you have at least 6 ranks in Piloting, you can spend 1 Resolve
              Point to move your starship up to 1-1/2 times its speed. You
              can make turns during this movement, but you add 2 to your
              starship’s distance between turns. 
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
                You maintain steady control over your starship
                during its movement and add only 1 to your starship’s distance
                between turns.
              </p>
            </div>
          </Collapse>
          <h4>Audacious Gambit (Helm Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowAudaciousGambit(!this.state.showAudaciousGambit)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showAudaciousGambit}>
            <div>
              <p>
              If you have at least 12 ranks in Piloting, you can spend 1 Resolve
              Point and attempt a Piloting check (DC = 15 + 2 × your starship’s
              tier) to pull off complex maneuvers. You can move your starship
              up to its speed, treating its distance between turns as if it were
              2 lower (minimum 0). You can also fly through hexes occupied
              by enemy vessels without provoking free attacks. At the end of
              your starship’s movement, you can rotate your starship to face
              in any direction. If you fail the check, you instead move as if you
              had taken the fly action (but still lose the Resolve Point).
              </p>
              <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
                Your incredible maneuvering leaves enemy
                gunners confounded. You gain a +4 circumstance bonus to your
                ship’s AC and TL until the start of the next round. 
              </p>
            </div>
          </Collapse> 
        </Modal.Body>
        <Modal.Footer>
          <Button name="pilot" onClick={this.props.onCloseRequest}>Close</Button>
        </Modal.Footer>
    </Modal>);
    }
}

export default PilotModal;