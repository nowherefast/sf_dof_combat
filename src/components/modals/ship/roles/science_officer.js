import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse';

class ScienceOfficerModal extends React.Component {
    constructor(props){
       super(props);
       this.state = {
         showBalance: false,
         showScan: false,
         showTargetSystem: false,
         showLockOn: false,
         showImproveCountermeasures: false
       }
    }

    toggleShowBalance(isBalanceShown){
      //Just close modal for now
      let newState = this.state;
      newState.showBalance = isBalanceShown;
      this.setState(newState);
    }
    toggleShowScan(isScanShown){
      //Just close modal for now
      let newState = this.state;
      newState.showScan = isScanShown;
      this.setState(newState);
    }
    toggleShowTargetSystems(isTargetSystemsShown){
      //Just close modal for now
      let newState = this.state;
      newState.showTargetSystem = isTargetSystemsShown;
      this.setState(newState);
    }
    toggleShowLockOn(isLockOnShown){
      //Just close modal for now
      let newState = this.state;
      newState.showLockOn = isLockOnShown;
      this.setState(newState);
    }
    toggleImproveCountermeasures(isImproveCountermeasuresShown){
      //Just close modal for now
      let newState = this.state;
      newState.showImproveCountermeasures = isImproveCountermeasuresShown;
      this.setState(newState);
    }

    render() {
        return(<Modal show={this.props.showModal} size="lg" onHide={this.props.onCloseRequest}>
        <Modal.Header>
          <Modal.Title>Science Officer Actions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <h4>Balance (Helm Phase)&nbsp;&nbsp;
          <Button
            onClick={() => this.toggleShowBalance(!this.state.showBalance)}
            size="sm"
          >
                Details
          </Button>
        </h4>
        <hr/>
        <Collapse in={this.state.showBalance}>
          <div>
            <p>
              You can balance the shields, redirecting power from one quadrant
              to protect another. With a successful Computers check (DC = 15 +
              2 × your starship’s tier), you can shift Shield Points (SP) from the
              shield in one quadrant to the shield in another quadrant, including
              to depleted shields (after rebalancing, every shield must have at
              least 10% of the total current SP). Alternatively, you can add up the
              SP from all the remaining shields and evenly distribute them to all
              four quadrants, putting any excess SP in the forward quadrant.
            </p>
            <p>
              <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
              While rebalancing the shields, you discover latent
              power in the shield systems. Before redistributing the Shield
              Points, you increase the total Shield Points by 5% of the
              starship’s PCU rating, up to the shields’ maximum value.
            </p>
          </div>
        </Collapse>
        <h4>Scan (Helm Phase)&nbsp;&nbsp;
          <Button
            onClick={() => this.toggleShowScan(!this.state.showScan)}
            size="sm"
          >
                Details
          </Button>
        </h4>
        <hr/>
        <Collapse in={this.state.showScan}>
          <div>
            <p>
              You can scan a starship with your sensors to learn information
              about it. This action requires your starship to have sensors (see
              page 300). You must attempt a Computers check, applying
              any modifiers from the starship’s sensors. You can attempt
              this check untrained. The DC for this check is equal to 10 + the
              tier of the starship being scanned + its bonus from defensive
              countermeasures (see page 298). If you succeed at this check,
              you learn the first unknown piece of information on the following
              list. For every 5 by which you exceed the check, you learn another
              unknown piece of information. Subsequent checks reveal new
              pieces of information, continuing down this list.       
            </p>
            <ol>
              <li><strong>Basic Information:</strong> Living crew complement and ship
              classification, size, speed, and maneuverability.</li>
              <li><strong>Defenses:</strong> AC, TL, total and current Hull Points, total and
              current Shield Points in each quadrant, and core PCU value.</li>
              <li><strong>Weapon:</strong> Information about one weapon, including its firing
              arc and the damage it deals, starting with the weapon that
              uses the most PCU. Repeat this entry until all the starship’s
              weapons are revealed.</li>
              <li><strong>Load:</strong> Information about how the starship’s expansion bays
              are allocated and any cargo the starship might be carrying.</li>
              <li><strong>Other:</strong> Any remaining ship statistics.</li>
            </ol>
            <p>
              <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
              Your insightful scans reveal a weakness in a random
              quadrant of the targeted starship. The next time one of your
              starship’s weapons deals damage to the targeted starship’s Hull
              Points, it has a 25% chance to also deal critical damage to a
              random system.
            </p>
          </div>
        </Collapse>
        <h4>Target System (Helm Phase, Push)&nbsp;&nbsp;
          <Button
            onClick={() => this.toggleShowTargetSystems(!this.state.showTargetSystem)}
            size="sm"
          >
                Details
          </Button>
        </h4>
        <hr/>
        <Collapse in={this.state.showTargetSystem}>
          <div>
          <p>
            You can use your starship’s sensors to target a specific system
            on an enemy starship. This action requires your starship to have
            sensors. You must attempt a Computers check, applying any
            modifiers from the starship’s sensors. The DC equals 15 + the tier of
            the enemy starship + its bonus from defensive countermeasures
            (see page 298). If you succeed, choose one system (core, engines,
            life support, sensors, or weapons). The next attack made by your
            starship that hits the enemy ship scores a critical hit on a natural
            roll of 19 or 20. If that attack deals critical damage, it affects the
            chosen system. For any further critical damage resulting from the
            attack, determine which system is affected randomly as normal.
            Your starship’s sensors can target only one system on a specific
            enemy starship at a time, though this action can be used to
            concurrently target systems on multiple starships.
          </p>
          <p>
            <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
            Your sensors keep a continuous lock on the
            enemy starship. The effects of target system last until the start
            of the next round.
          </p>
          </div>
        </Collapse>
        <h4>Lock On (Helm Phase, Push)&nbsp;&nbsp;
          <Button
            onClick={() => this.toggleShowLockOn(!this.state.showLockOn)}
            size="sm"
          >
                Details
          </Button>
        </h4>
        <hr/>
        <Collapse in={this.state.showLockOn}>
          <div>
            <p>
              If you have at least 6 ranks in Computers, you can lock your
              starship’s targeting system on to one enemy vessel. You must
              spend 1 Resolve Point and attempt a Computers check. The DC
              equals 15 + the tier of the target starship + its bonus from defensive
              countermeasures (see page 298). If you succeed, your starship’s
              gunners gain a +2 bonus to gunnery checks against the target for
              the rest of the round. This action can be taken only once per round.
            </p>
            <p>
              <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
              Your weapon lock greatly improves your gunners’
              accuracy. Until the start of the next round, any attacks by your
              starship score a critical hit on a natural roll of 19 or 20.
            </p>
          </div>
        </Collapse>
        <h4>Improve Countermeasures (Helm Phase)&nbsp;&nbsp;
          <Button
            onClick={() => this.toggleImproveCountermeasures(!this.state.showImproveCountermeasures)}
            size="sm"
          >
                Details
          </Button>
        </h4>
        <hr/>
        <Collapse in={this.state.showImproveCountermeasures}>
          <div>
            <p>
              If you have at least 12 ranks in Computers, you can try to foil
              enemy targeting arrays and incoming projectiles by spending
              1 Resolve Point and attempting a Computers check. The DC
              equals 10 + 2 × the tier of the target starship + its bonus from
              defensive countermeasures (see page 298). If you’re successful,
              gunners aboard the target starship roll twice and take the worse
              result for gunnery checks during this round (including checks for
              tracking weapons).
            </p>
            <p>
              <strong>Optional Rule [Starship Operations Manual][Critical]:</strong>
              Your powerful countermeasures
              send false signals to the enemy targeting systems. Gunners
              aboard the target starship also take a –2 penalty to gunnery
              checks during this round.
            </p>
          </div>
        </Collapse>
        </Modal.Body>
        <Modal.Footer>
          <Button name="science_officer" onClick={this.props.onCloseRequest}>Close</Button>
        </Modal.Footer>
    </Modal>);
    }
}

export default ScienceOfficerModal;