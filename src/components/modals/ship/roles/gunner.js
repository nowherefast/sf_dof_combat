import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse';

class GunnerModal extends React.Component {
    constructor(props){
       super(props);
       this.state = {
         showFireAtWill: false,
         showShoot: false,
         showBroadside: false,
         showPreciseTargeting: false
       }
    }

    toggleShowFireAtWill(isFireAtWillShown){
      //Just close modal for now
      let newState = this.state;
      newState.showFireAtWill = isFireAtWillShown;
      this.setState(newState);
    }
    toggleShowShoot(isShootShown){
      //Just close modal for now
      let newState = this.state;
      newState.showShoot = isShootShown;
      this.setState(newState);
    }
    toggleShowBroadside(isBroadsideShown){
      //Just close modal for now
      let newState = this.state;
      newState.showBroadside = isBroadsideShown;
      this.setState(newState);
    }
    toggleShowPreciseTargeting(isPreciseTargetingShown){
      //Just close modal for now
      let newState = this.state;
      newState.showPreciseTargeting = isPreciseTargetingShown;
      this.setState(newState);
    }

    render() {
        return(<Modal show={this.props.showModal} size="lg" onHide={this.props.onCloseRequest}>
        <Modal.Header>
          <Modal.Title>Gunner Actions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Fire at Will (Gunnery Phase, Push)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowFireAtWill(!this.state.showFireAtWill)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showFireAtWill}>
            <p>
            You can fire any two starship weapons, regardless of their arc.
            Each attack is made at a –4 penalty.
            </p>
          </Collapse>
          <h4>Shoot (Gunnery Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowShoot(!this.state.showShoot)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showShoot}>
            <p>
            You can fire one of your starship’s weapons. If you use a turret
            weapon, you can target a ship in any arc.
            </p>
          </Collapse>
          <h4>Broadside (Gunnery Phas, Push)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowBroadside(!this.state.showBroadside)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showBroadside}>
            <p>
            At 6th level, you can expend 1 Resolve Point to fire all of the
            starship weapons mounted in one arc (including turret-mounted
            weapons). Each weapon can target any vessel in that arc. All of
            these attacks are made with a –2 penalty.
            </p>
          </Collapse>
          <h4>Precise Targeting (Gunnery Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowPreciseTargeting(!this.state.showPreciseTargeting)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showPreciseTargeting}>
            <p>
            At 12th level, you can perform a very precise strike by spending
            1 Resolve Point and firing one starship weapon at a single target.
            If the attack hits and the enemy ship’s shields on that quadrant
            are depleted before your attack, you deal critical damage to a
            random system. If the attack would normally cause critical
            damage, the normal critical damage applies as well (meaning
            your attack could potentially deal critical damage multiple times;
            determine which system is damaged as normal each time).
            </p>
          </Collapse> 
        </Modal.Body>
        <Modal.Footer>
          <Button name="gunner" onClick={this.props.onCloseRequest}>Close</Button>
        </Modal.Footer>
    </Modal>);
    }
}

export default GunnerModal;