import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse';
import Table from 'react-bootstrap/Table';

class EngineerModal extends React.Component {
    constructor(props){
       super(props);
       this.state = {
        showDivert: false,
        showHoldItTogether: false,
        showPatch: false,
        showOverpower: false,
        showQuickFix: false 
       }
    }

    toggleShowDivert(isDivertShown){
        //Just close modal for now
        let newState = this.state;
        newState.showDivert = isDivertShown;
        this.setState(newState);
    }
    toggleShowHoldItTogether(isHoldItTogetherShown){
        //Just close modal for now
        let newState = this.state;
        newState.showHoldItTogether = isHoldItTogetherShown;
        this.setState(newState);
    }
    toggleShowPatch(isPatchShown){
        //Just close modal for now
        let newState = this.state;
        newState.showPatch = isPatchShown;
        this.setState(newState);
    }
    toggleShowOverpower(isOverpowerShown){
        //Just close modal for now
        let newState = this.state;
        newState.showOverpower = isOverpowerShown;
        this.setState(newState);
    }
    toggleShowQuickfix(isQuickfixShown){
        //Just close modal for now
        let newState = this.state;
        newState.showQuickFix = isQuickfixShown;
        this.setState(newState);
    }

    render() {
        return(<Modal show={this.props.showModal} size="lg" onHide={this.props.onCloseRequest}>
        <Modal.Header>
          <Modal.Title>Engineer Actions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Divert (Engineering Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowDivert(!this.state.showDivert)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showDivert}>
            <div>
            <p>
            You can divert auxiliary power into one of your starship’s
            systems, giving it a boost. This requires a successful Engineering
            check (DC = 10 + 2 × your starship’s tier), and the results depend
            on where you decide to send this extra power. If you send it to
            the engines, your starship’s speed increases by 2 this round. If
            you send it to the science equipment, all science officers receive
            a +2 bonus to their crew actions this round. If you send it to
            the starship’s weapons, treat each damage die that rolls a 1 this
            round as having rolled a 2 instead. If you send it to the shields,
            restore an amount of Shield Points equal to 5% of the PCU rating
            of the starship’s power core (see page 296), up to the shields’
            maximum value. Evenly distribute the restored Shield Points
            to all four quadrants
            </p>
            <p>
                <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                For a crucial moment, you far surpass the power core’s
                potential output. The results of a critical divert action depend
                on where you decided to send the extra power.
            </p>
            <ul>
                <li>Engines: You also divert power to the maneuvering thrusters,
                lowering the turn value of your starship by 1 this round.</li>
                <li>Science Equipment: You’re able to divert power in an efficient
                and balanced way. Science officers can roll their checks twice
                this round and use the better result.</li>
                <li>Shields: You’re able to efficiently route energy to the shields,
                doubling the number of Shield Points restored to 10% of the
                PCU rating of the starship’s power core. If this would restore the
                shields over their maximum value, these excess Shield Points
                remain until the beginning of the next engineering phase, at
                which point any excess Shield Points are lost.</li>
                <li>Starship Weapons: The augmented weapons run at maximum
                safe power. For your starship’s weapons, treat each damage
                die roll that results in a 1 this round as having rolled the die’s
                maximum result instead.</li>
            </ul>
            </div>
          </Collapse>

          <h4>Hold It Together (Engineering Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowHoldItTogether(!this.state.showHoldItTogether)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showHoldItTogether}>
            <div>
                <p>
                You can hold one system together by constantly patching and
                modifying it. If you succeed at an Engineering check (DC = 15 +
                2 × your starship’s tier), you can select one system; that system
                is treated as if its critical damage condition were two steps less
                severe for the rest of the round (wrecked becomes glitching, and
                a malfunctioning or glitching system functions as if it had taken
                no critical damage). This check isn’t modified by penalties from
                critical damage to the power core.
                </p>
                <p>
                    <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                    Thanks to your inspired repairs, the system
                    you worked on is treated as if its critical damage condition were
                    two steps less severe (wrecked becomes glitching, whereas
                    malfunctioning or glitching become undamaged) for 1d4 rounds.
                </p>
            </div>
          </Collapse>
          <h4>Patch (Engineering Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowPatch(!this.state.showPatch)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showPatch}>
            <div>
                <p>
                You can patch a system to reduce the effects of a critical damage
                condition. The number of actions and the DC of the Engineering
                check required to patch a system depend on how badly the
                system is damaged, as indicated on the table on page 324.
                Multiple engineers can pool their actions in a single round to
                effect repairs more quickly, but each engineer must succeed
                at her Engineering check to contribute her action to the patch.
                324 STARSHIPS
                The number of actions required can be reduced by 1 (to a
                minimum of 1 action) by increasing the DC by 5. If you succeed
                at this check, the severity of the critical damage is unchanged,
                but it is treated as one step less severe for the remainder of the
                combat, until 1 hour has passed, or until the system takes critical
                damage again (which removes the patch and applies the new
                severity). This action can be taken more than once per round,
                and this check is not modified by any critical damage to the core.
                </p>
                <p>
                    <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                    Your patch was especially robust. If the system takes
                    critical damage again, it removes the patch but doesn’t also
                    apply additional critical damage.
                </p>
                <p>
                    <Table>
                        <thead>
                            <tr>
                            <th>Dmg Condition</th>
                            <th># Actions(Patch)</th>
                            <th>DC</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> Glitching </td>
                                <td> 1 </td>
                                <td> 10 + 2 × your starship’s tier </td>
                            </tr>
                            <tr>
                                <td> Malfunctioning </td>
                                <td> 2 </td>
                                <td> 15 + 2 × your starship’s tier </td>
                            </tr>
                            <tr>
                                <td> Wrecked </td>
                                <td> 3 </td>
                                <td> 20 + 2 × your starship’s tier</td>
                            </tr>
                        </tbody>
                    </Table>
                </p>
            </div>
          </Collapse>
          <h4>Overpower (Engineering Phase, Push)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowOverpower(!this.state.showOverpower)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showOverpower}>
            <div>
                <p>
                If you have at least 6 ranks in Engineering, you can spend 1 Resolve
                Point and attempt an Engineering check (DC = 10 + 3 × your
                starship’s tier) to squeeze more out of your ship’s systems. If you’re
                successful, this functions as the divert action, but you can send
                extra power to any three systems listed in that action. This action
                and the divert action can’t be taken in the same round. 
                </p>
                <p>
                    <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                    Choose one of the three systems to which you
                    diverted extra power. One of those systems also receives the
                    critical effect benefit of the divert action.
                </p>
            </div>
          </Collapse>
          <h4>Quick Fix (Engineering Phase)&nbsp;&nbsp;
            <Button
                onClick={() => this.toggleShowQuickfix(!this.state.showQuickFix)}
                size="sm"
            >
                Details
            </Button>
          </h4>
          <hr/>
          <Collapse in={this.state.showQuickFix}>
            <div>
                <p>
                If you have at least 12 ranks in Engineering, you can try to repair
                a system quickly by spending 1 Resolve Point and attempting an
                Engineering check (DC = 15 + 2 × you starship’s tier). If successful,
                you remove the critical damage condition from one system
                for 1 hour (allowing it to function as if it had taken no critical
                damage), after which time it must be repaired as normal.
                </p>
                <p>
                    <strong>Optional Rule [Starship Operations Manual][Critical]: </strong>  
                    Your inspired engineering results in a lasting
                    repair, removing the critical damage condition from the system
                    for 1 day instead of for 1 hour.
                </p>
            </div>
          </Collapse>
        </Modal.Body>
        <Modal.Footer>
          <Button name="engineer" onClick={this.props.onCloseRequest}>Close</Button>
        </Modal.Footer>
    </Modal>);
    }
}

export default EngineerModal;