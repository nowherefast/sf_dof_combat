import React from 'react';

//D3 Stuff
import { select, selectAll } from 'd3-selection'
import { scaleLinear, scaleOrdinal } from 'd3-scale'
import { max, range } from 'd3-array'
import { format } from 'd3-format'
import { line, arc, curve } from 'd3-shape'
import * as d3 from "d3";

//My CSS stuff

/*
 * ==============================
 * Constants
 * ==============================
 */
const SVG_SHIELDS = {
    svg_width: 600,
    svg_height: 600,
    circleWidth: 400, //Width of the circle
    circleHeight: 400,	//Height of the circle
    dotRadius: 4,
    margin: {top: 20, right: 20, bottom: 20, left: 20}, //The margins of the SVG
    labelFactor: 1.25, //How much farther than the radius of the outer circle should the labels be placed
    wrapWidth: 60, //The number of pixels after which a label needs to be given a new line
    minLevel: 0,
    opacityCircles: 1, //The opacity of the circles of each blob
    strokeWidth: 2,
    radialHistogram: {
        opacityCompanyHistogram: 0.10,
        opacityIndividualHistogram: 0.10 //The opacity of the area of the blob
    }
}
const SHIELD_DETAILS = {
    arcs: ['Forward','Starboard','Aft','Port'],
    display_levels: 5
}





class ShipShields extends React.Component {
  constructor(props){
     super(props);
     this.state = {
     };
     this.createGraph = this.createGraph.bind(this);
     this.createCircularGridAndAxes = this.createCircularGridAndAxes.bind(this);
     this.createShipShields = this.createShipShields.bind(this);
  }

  /*
   * ==============================
   * Controlling creation
   * ==============================
   */
  createGraph(){
    const node = this.node;

    //remove all previous elements assoc. with graoh, so it can be recreated
    select(node).selectAll("*").remove();
    let grid = this.createCircularGridAndAxes(select(node));
    let createShipShields = this.createShipShields(grid);
  }

  /*
   * ==============================
   * Graph Axis Setup
   * ==============================
   */
  createCircularGridAndAxes(parent){
    let radius = Math.min(SVG_SHIELDS.circleWidth/2, SVG_SHIELDS.circleHeight/2);
    let radiansOfSlice = Math.PI * 2 / SHIELD_DETAILS.arcs.length;
    let rScale = scaleLinear()
  		.range([0, radius])
  		.domain([0, SHIELD_DETAILS.display_levels]);
    let allAxis = (SHIELD_DETAILS.arcs.map(function(a){return a}))

    let circularGridAndAxis = parent.append("g")
      .attr("class", "axisWrapper")

    circularGridAndAxis
      .selectAll(".levels")
      .data( range(0, SHIELD_DETAILS.display_levels).reverse() )
      .enter()
      .append("circle")
        .attr("class", "gridCircle")
        .attr("r", (d, i) => { return radius/SHIELD_DETAILS.display_levels*d; })
        .style("fill", "#CDCDCD")
        .style("stroke", "#FFFFFF")
        .style("fill-opacity", SVG_SHIELDS.opacityCircles)


    circularGridAndAxis.selectAll(".upAxisLabel")
	   .data( range(0, SHIELD_DETAILS.display_levels).reverse() )
	   .enter()
     .append("text")
  	   .attr("class", "upAxisLabel")
  	   .attr("x", -25)
  	   .attr("y", (d) => { return -d * radius/SHIELD_DETAILS.display_levels; })
  	   .attr("dy", "0.4em")
  	   .style("font-size", "10px")
       .style("fill-opacity", 1)
       .style("z-index", -1000)
       .attr("fill", "#ff0000")
  	   .text( (d, i) => { return d * 25 + '%'});

    let axis = circularGridAndAxis.selectAll(".axis")
   		.data(allAxis)
   		.enter()
   		.append("g")
   		.attr("class", "axis");


    let display_levels = SHIELD_DETAILS.display_levels;

    axis.append("line")
  		.attr("x1", 0)
  		.attr("y1", 0)
  		.attr("x2", function(d, i){ return rScale( display_levels * 1.1) * Math.cos(radiansOfSlice*i - Math.PI/2); })
  		.attr("y2", function(d, i){ return rScale( display_levels * 1.1) * Math.sin(radiansOfSlice*i - Math.PI/2); })
  		.attr("class", "line")
  		.style("stroke", "#1c3465")
  		.style("stroke-width", "2px");

    //Append the labels at each axis
    let withinScopeLabelFactor = SVG_SHIELDS.labelFactor;

  	axis.append("text")
  		.attr("class", "legend")
  		.style("font-size", "11px")
      .style("fill", "#1c3465")
  		.attr("text-anchor", "middle")
  		.attr("dy", "0.35em")
  		.attr("x", function(d, i){ return rScale(display_levels * withinScopeLabelFactor) * Math.cos(radiansOfSlice*i - Math.PI/2); })
  		.attr("y", function(d, i){ return rScale(display_levels * withinScopeLabelFactor) * Math.sin(radiansOfSlice*i - Math.PI/2); })
  		.text(function(d){ return d})
  		.call(wrap, SVG_SHIELDS.wrapWidth);

    return axis;
  }
  

  createShipShields(parent){
    if(this.props.data.length == 0) return;
    let outerRange = Math.min(SVG_SHIELDS.circleWidth/2, SVG_SHIELDS.circleHeight/2);
    let outerRadius = outerRange - ((1/SHIELD_DETAILS.display_levels) * outerRange);
    //let innerRadius = outerRadius / this.props.numLevels; //imagine a ring.  This is the radius of the innermost point of ring (starts at center of circle)
    let innerRadius = 0;
    let radiansOfSlice = Math.PI * 2 / SHIELD_DETAILS.arcs.length;
    let outerScale = scaleLinear().range([innerRadius, outerRadius]).domain([SVG_SHIELDS.minLevel, SHIELD_DETAILS.display_levels - 1]);
    let numShieldArcs = SHIELD_DETAILS.arcs.length;
    let shiftValue = 2 * Math.PI

    let rScale = scaleLinear()
     .range([0, outerRange])
     .domain([0, SHIELD_DETAILS.display_levels]);

    let histogramBorder = d3.lineRadial().curve(d3.curveLinearClosed)
      .radius(function(d) { return rScale(d.value); })
      .angle(function(d,i) {	return i*radiansOfSlice; });

    let arc = d3.arc()
      .innerRadius(function (d, i) {
        return innerRadius;
      })
      .outerRadius(function (d, i) {
        return outerScale(d.value);
      })
      .startAngle(function (d, i) {return 2 * Math.PI * ( (2*i - 1) / (2*numShieldArcs) );}) // i=0,1,2,3,4
      .endAngle(function (d, i) {return 2 * Math.PI * ( (2*i + 1) / (2*numShieldArcs) );});

    //console.log(this.props.companies);
    //create a wrapper for arcs because we're dealing with objects for axis:value
    let opacityIndividualHistogram = SVG_SHIELDS.radialHistogram.opacityIndividualHistogram;
    let strokeWidth = SVG_SHIELDS.strokeWidth;

    let arcWrapperGroup;
    let temp;
    let tempString;

    for(let index=0; index<this.props.data.length; index++){
        arcWrapperGroup = parent.selectAll(".individualArcWrapper_" + index)
         .data(this.props.data[index])
         .enter()
         .append("path")
           .attr("class", "individualArcWrapper_" + index)
           .attr("d", function(d,i) { return arc(d, i); })
           .style("fill", "#ADD8E6")
           .style("fill-opacity", opacityIndividualHistogram)
           .style("stroke-width", strokeWidth + "px")
           .style("stroke-opacity", "1")
           .style("stroke", "#000080")
           .on('mouseover', function (d){
                //Bring back this individual arc
                tempString = ".individualArcWrapper_" + index;
                temp = d3.selectAll(tempString)
                .transition().duration(200)
                .style("fill-opacity", 0.7);
            })
            .on('mouseout', function(){
                //Bring back all blobs
                d3.selectAll(".individualArcWrapper_" + index)
                .transition().duration(200)
                .style("fill-opacity", opacityIndividualHistogram);
            });
     }
  }






  /*
   * ==============================
   * REACT Rendering Methods
   * ==============================
   */
  componentDidMount(){
    if(this.props.data.length > 0)
        this.createGraph();
  }
  componentDidUpdate(){
    if(this.props.data.length > 0)
        this.createGraph();
  }

  render() {
    return (
      <div className="ShipShieldsGraph">
        <svg ref={node => this.node = node}
         width={SVG_SHIELDS.svg_width} height={SVG_SHIELDS.svg_height}
         viewBox={`-300 -300 ${SVG_SHIELDS.svg_width} ${SVG_SHIELDS.svg_height}`}>
        </svg>
      </div>
    );
  }
}

/////////////////////////////////////////////////////////
/////////////////// Helper Function /////////////////////
/////////////////////////////////////////////////////////

//Taken from http://bl.ocks.org/mbostock/7555321
//Wraps SVG text
function wrap(text, width) {
    text.each(function() {
      var text = select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = 1.4, // ems
          y = text.attr("y"),
          x = text.attr("x"),
          dy = parseFloat(text.attr("dy")),
          tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");
  
      while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(" "));
          if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
          tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
          }
      }
    });
  }//wrap

export default ShipShields;
