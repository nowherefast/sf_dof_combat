import React from 'react';
import VehicleCrewMemberModal from './modals/vehicle/forms/vehicle_crew_member.js';

/** Bootstrap */
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

class VehicleCombat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
           encounter_name: '',
           showModal:{
               'pilot': false,
               'createCrewmember': false
           },
           encounter_data:[],
           active_character: -1,
           active_vehicle: -1,
           land_list:[],
           vehicle_list:[]
        };
    }

    createCreateVehicleCrewMember(){
        let newState = this.state;
        // Create dummy object
        // Note: we required dummy "defaultId" for characterId, as we will be
        // checking for the last one when filling in data
        let VehicleCrewMember = {
            'characterId':'defaultId',
            'characterName':'',
            'playerName':'',
            'staminaMax': -1,
            'hp_max': -1,
            'staminaCurr': -1,
            'hp_curr': -1,
            'initiaveRoll': 0,
            'eac':-1,
            'kac':-1,
            'rangedAttack':-1,
            'isPiloting': false,
            'vehicle': '',
            'skills':{
                'acrobatics':0,
                'athletics':0,
                'piloting':0
            }
        }
        // All characters dummy objects will be initially unallocated to vehicles
        newState.land_list.push(VehicleCrewMember);
        // Reveal character creation modal
        newState.showModal.createCrewmember = true;
        this.setState(newState);
    }
    handleNewCrewMemberChange(secondary, e){
        let newState = this.state;
        let index = -1;
        // Find the index of the last dummy object
        for(let i=0; i<newState.land_list.length; i++){
            if(newState.land_list[i].characterId === "defaultId"){
                index++;
            }
        }
        // Assuming that index = -1, then that means there's no defaultId
        // So Id must have just been created, in which case we'll assume that
        // the last created character is most relevant
        if(index == -1){
            index = newState.land_list.length - 1;
            if(secondary === "skills"){
                newState.land_list[index].skills[e.target.name] = e.target.value;
            } else {
                newState.land_list[index][e.target.name] = e.target.value;
            }
        } else {
            if(secondary === "skills"){
                newState.land_list[index].skills[e.target.name] = e.target.value;
            } else {
                newState.land_list[index][e.target.name] = e.target.value;
            }
        } 
        this.setState(newState);
    }



    /*
    createCreateVehicleCrewMember(e){
        let newState = this.state;
        newState.showCreateEditCharacter = true;
        newState.active_vehicle = -1;
    
        //Then create a new dummy object
        let VehicleCrewMember = {
            'characterId':'',
            'characterName':'',
            'playerName':'',
            'staminaMax': -1,
            'hp_max': -1,
            'staminaCurr': -1,
            'hp_curr': -1,
            'initiaveRoll': 0,
            'eac':-1,
            'kac':-1,
            'rangedAttack':-1,
            'isPiloting': false,
            'vehicle': '',
            'skills':{
                'acrobatics':0,
                'athletics':0,
                'piloting':0
            }
        }
        if(newState.vehicle_list.length != 0){
            for(let i=0; i<newState.vehicle_list.length; i++){
                if(newState.vehicle_list[i].vehicle_id === e.vehicle_id){
                    newState.active_vehicle = i;
                    newState.vehicle_list[i].crew.push(VehicleCrewMember);
                    break;
                }
            }        
        }
        if(newState.active_vehicle == -1)
        {
            newState.land_list.push(VehicleCrewMember);
        }
        this.setState(newState);
    }
    */

    /** CHARACTER HANDLING */
    addCharacter(){
        let newState = this.state;
        newState.showModal.createCrewmember = false;
        this.setState(newState);
    }
    cancelCreateCharacterModal(){
        let newState = this.state;
        newState.showModal.createCrewmember = false;
        newState.land_list.pop();
        this.setState(newState);
    }
    closeCreateCharacterModal(){
        let newState = this.state;
        newState.showModal.createCrewmember = false;
        this.setState(newState);
    }


    render() {
        return(
            <div>
                <Container>
                    <Row>
                    <Col xs={{ order: 'first' }}><Button onClick={() => this.createCreateVehicleCrewMember()}>Character +</Button></Col>
                    </Row>
                </Container>


                {/** Character Creation Form */}
                <VehicleCrewMemberModal showModal={this.state.showModal.createCrewmember}
                    onCancelRequest={() => this.cancelCreateCharacterModal()}
                    onCharacterChange={(secondary, e) => this.handleNewCrewMemberChange(secondary, e)}
                    onAddCharacter={() => this.addCharacter()}
                />
            </div>
        );
    }
}
export default VehicleCombat;

/**
 * 
            onCrewChange={(secondary, e) => this.handleCrewChange(secondary, e)}
            onCrewDetailChange={(e) => this.handleCrewDetailChange(e)}
            onImportCharacterData={(e) => this.importCharacterData(e)}
            onDownloadRequest={() => this.downloadCharacter()}
            onAddCharacterRequest={() => this.addCharacter()}
            onCancelRequest={() => this.cancelCreateCharacterModal()}
 */