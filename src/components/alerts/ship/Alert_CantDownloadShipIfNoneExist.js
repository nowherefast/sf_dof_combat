import React from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

class Alert_CantDownloadShipIfNoneExist extends React.Component {
    constructor(props){
       super(props);
    }
    render() {
        return(    
        <>
            <Alert show={this.props.showAlert} variant="danger">
              <Alert.Heading>Can't download a ship if none exist.</Alert.Heading>
              <p>
                Can't download a ship if none exist. Please create a ship and try again.
              </p>
              <hr />
              <div className="d-flex justify-content-end">
                <Button onClick={this.props.onCloseRequest} variant="outline-danger">
                  Close
                </Button>
              </div>
            </Alert>
          </>
        );
    }
}
export default Alert_CantDownloadShipIfNoneExist;