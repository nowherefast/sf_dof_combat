import React from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

class Alert_CantCreatePilotWithoutShip extends React.Component {
    constructor(props){
       super(props);
    }
    render() {
        return(    
        <>
            <Alert show={this.props.showAlert} variant="danger">
              <Alert.Heading>Can't create a pilot without a ship</Alert.Heading>
              <p>
                Sorry, you can't create a pilot without a ship first.  I know, it sucks.
                Please create a ship and try again.
              </p>
              <hr />
              <div className="d-flex justify-content-end">
                <Button onClick={this.props.onCloseRequest} variant="outline-danger">
                  Close
                </Button>
              </div>
            </Alert>
          </>
        );
    }
}
export default Alert_CantCreatePilotWithoutShip;