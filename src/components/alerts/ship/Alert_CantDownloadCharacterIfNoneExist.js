import React from 'react';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

class Alert_CantDownloadCharacterIfNoneExist extends React.Component {
    constructor(props){
       super(props);
    }
    render() {
        return(    
        <>
            <Alert show={this.props.showAlert} variant="danger">
              <Alert.Heading>Can't Download a Character if none exist</Alert.Heading>
              <p>
                Can't download a character if none exist.
              </p>
              <hr />
              <div className="d-flex justify-content-end">
                <Button onClick={this.props.onCloseRequest} variant="outline-danger">
                  Close
                </Button>
              </div>
            </Alert>
          </>
        );
    }
}
export default Alert_CantDownloadCharacterIfNoneExist;