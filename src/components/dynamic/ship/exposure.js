import React from 'react';
import Form from 'react-bootstrap/Form';

import '../../../css/Ship.css'

class Exposure extends React.Component {
    constructor(props){
       super(props);
    }
    /**
key={character.characterId}
onClick={(e) => this.handleCharacterClick(character.characterId, e)}
onClick={(e) => this.handleCharacterDeleteClick(character, e)}
onChange={(e) => this.handleCrewRoleChange(character, e)}
value={character.ship_role}
     */
    render() {
        return(
            <Form>
                <Form.Group controlId="ShipDetailsCheckboxes">
                    <Form.Check type="checkbox" label="Basic" name="basic_information" onChange={this.props.onExposureCheck} checked={this.props.ship_active.exposure.basic_information}/>
                    <Form.Check type="checkbox" label="Defenses" name="defenses" onChange={this.props.onExposureCheck} checked={this.props.ship_active.exposure.defenses} />
                    <Form.Check type="checkbox" label="Load" name="load"  onChange={this.props.onExposureCheck} checked={this.props.ship_active.exposure.load}/>
                    <Form.Check type="checkbox" label="Other" name="other"  onChange={this.props.onExposureCheck} checked={this.props.ship_active.exposure.other}/>
                    <hr/>
                    <Form.Check type="checkbox" label="Turrets" name="wpns_turrets" onChange={(e) => this.props.onComplexExposureCheck('weapons', e)} checked={this.props.ship_active.exposure.weapons.wpns_turrets}/>
                    <Form.Check type="checkbox" label="Forward_Wpns" name="wpns_forward" onChange={(e) => this.props.onComplexExposureCheck('weapons', e)} checked={this.props.ship_active.exposure.weapons.wpns_forward} />
                    <Form.Check type="checkbox" label="Port_Wpns" name="wpns_port"  onChange={(e) => this.props.onComplexExposureCheck('weapons', e)} checked={this.props.ship_active.exposure.weapons.wpns_port}/>
                    <Form.Check type="checkbox" label="Starboard_Wpns" name="wpns_starboard"  onChange={(e) => this.props.onComplexExposureCheck('weapons', e)} checked={this.props.ship_active.exposure.weapons.wpns_starboard}/>
                    <Form.Check type="checkbox" label="Aft_Wpns" name="wpns_aft"  onChange={(e) => this.props.onComplexExposureCheck('weapons', e)} checked={this.props.ship_active.exposure.weapons.wpns_aft}/> 
                    <hr/>
                    <Form.Check type="checkbox" label="Ability 1" name="spec_ability_1" onChange={(e) => this.props.onComplexExposureCheck('special_abilities', e)} checked={this.props.ship_active.exposure.special_abilities.spec_ability_1}/>
                    <Form.Check type="checkbox" label="Ability 2" name="spec_ability_2" onChange={(e) => this.props.onComplexExposureCheck('special_abilities', e)} checked={this.props.ship_active.exposure.special_abilities.spec_ability_2} />
                    <Form.Check type="checkbox" label="Ability 3" name="spec_ability_3"  onChange={(e) => this.props.onComplexExposureCheck('special_abilities', e)} checked={this.props.ship_active.exposure.special_abilities.spec_ability_3}/>
                    <Form.Check type="checkbox" label="Ability 4" name="spec_ability_4"  onChange={(e) => this.props.onComplexExposureCheck('special_abilities', e)} checked={this.props.ship_active.exposure.special_abilities.spec_ability_4}/>
                    <Form.Check type="checkbox" label="Ability 5" name="spec_ability_5"  onChange={(e) => this.props.onComplexExposureCheck('special_abilities', e)} checked={this.props.ship_active.exposure.special_abilities.spec_ability_5}/>
                    <Form.Check type="checkbox" label="Ability 6" name="spec_ability_6"  onChange={(e) => this.props.onComplexExposureCheck('special_abilities', e)} checked={this.props.ship_active.exposure.special_abilities.spec_ability_6}/>                                 
                </Form.Group>
            </Form>
        );
    }
}

export default Exposure;