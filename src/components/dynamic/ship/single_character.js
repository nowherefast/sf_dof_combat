import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Image from 'react-bootstrap/Image';
import Form from 'react-bootstrap/Form';

import '../../../css/Ship.css'
import TrashIcon from '../../../img/trash-var-solid.png'

class SingleCharacter extends React.Component {
    constructor(props){
       super(props);
    }
    /**
key={character.characterId}
onClick={(e) => this.handleCharacterClick(character.characterId, e)}
onClick={(e) => this.handleCharacterDeleteClick(character, e)}
onChange={(e) => this.handleCrewRoleChange(character, e)}
value={character.ship_role}
     */
    render() {
        return(
            <div>
                <ButtonGroup aria-label="CharacterButtonGroup">
                    <Button 
                        variant="dark"
                        onClick={(e) => this.props.onCharacterClick(this.props.character.characterId, e)}
                        ><h5>{this.props.character.characterName}</h5></Button>
                    <Button 
                        variant="light"
                        onClick={(e) => this.props.onCharacterDelete(this.props.character, e)}>
                        <Image src={TrashIcon} className="icon_size" roundedCircle />
                    </Button>
                </ButtonGroup>
                <Form.Group controlId="characterRoleGrp">
                    <Form.Label>Role</Form.Label>
                    <Form.Control as="select" name='ship_role' 
                        onChange={(e) => this.props.onCrewRoleChange(this.props.character, e)}
                        value={this.props.character.ship_role}>
                        <option value='captain'>Captain</option>
                        <option value='engineer'>Engineer</option>
                        <option value='gunner'>Gunner</option>
                        <option value='pilot'>Pilot</option>
                        <option value='science_officer'>Science Officer</option>
                    </Form.Control>
                </Form.Group>
            </div>
        );
    }
}

export default SingleCharacter;