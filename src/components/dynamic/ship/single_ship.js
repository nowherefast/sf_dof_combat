import React from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Image from 'react-bootstrap/Image';

import '../../../css/Ship.css'
import TrashIcon from '../../../img/trash-var-solid.png'

class SingleShip extends React.Component {
    constructor(props){
       super(props);
    }
    /**
     * variant={ship.disposition}
     * onClick={(e) => this.handleShipSelectClick(ship.ship_id, e)}><h4>{ship.stats.ship_name}</h4></Button>
     * onClick={(e) => this.handleShipDeleteClick(ship.ship_id, e)}>
     */
    render() {
        return(
            <ButtonGroup aria-label="ShipButton">
                <Button 
                    variant={this.props.ship.disposition}
                    onClick={(e) => this.props.onShipSelect(this.props.ship.ship_id, e)}><h4>{this.props.ship.stats.ship_name}</h4></Button>
                <Button 
                    variant="light"
                    onClick={(e) => this.props.onShipDelete(this.props.ship.ship_id, e)}>
                <Image src={TrashIcon} className="icon_size" roundedCircle /></Button>
            </ButtonGroup>
        );
    }
}

export default SingleShip;