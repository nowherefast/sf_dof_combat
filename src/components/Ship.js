import React from 'react';
import { useState } from 'react';
/* API Communication */
import axios from "axios";

/* CSS */
import '../css/Ship.css';

/* Ship Role Modals */
import ShipShields from './ShipShields.js';
import CaptainModal from './modals/ship/roles/captain.js';
import PilotModal from './modals/ship/roles/pilot.js';
import EngineerModal from './modals/ship/roles/engineer.js';
import GunnerModal from  './modals/ship/roles/gunner.js';
import ScienceOfficerModal from './modals/ship/roles/science_officer.js';

/* Dynamic Elements */
import SingleShip from './dynamic/ship/single_ship.js';
import SingleCharacter from './dynamic/ship/single_character.js';
import Exposure from './dynamic/ship/exposure.js';

/* Forms, Alerts, & Other Modals*/
import Alert_CantCreatePilotWithoutShip from './alerts/ship/Alert_CantCreatePilotWithoutShip.js';
import Alert_CantDownloadCharacterIfNoneExist from './alerts/ship/Alert_CantDownloadCharacterIfNoneExist.js';
import Alert_CantDownloadShipIfNoneExist from './alerts/ship/Alert_CantDownloadShipIfNoneExist.js';
import CharacterModal from './modals/ship/forms/character.js';
import EncounterLoadModal from './modals/ship/forms/encounter_load.js';
import EncounterSaveModal from './modals/ship/forms/encounter_save.js';
import DamageReportModal from './modals/ship/forms/damage_report.js';
import ShipCreateModal from './modals/ship/forms/ship_create.js';
import SpecialAbilitiesList from './modals/ship/forms/special_abilities.js';
import ShieldForm from './combat/ship/shield_form.js';
import DamageForm from './combat/ship/damage_form.js';
import HullForm from './combat/ship/hull_form.js';

/*REACT Bootstrap Stuff*/
import Alert from 'react-bootstrap/Alert';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from  'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import InputGroup from 'react-bootstrap/InputGroup';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Image from 'react-bootstrap/Image';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Tootip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

import AddCharacterIcon from '../img/AddCharacter.png';
import CreateSpaceshipIcon from '../img/CreateSpaceship.png';
import DownloadIcon from '../img/download.png';
import DowloadCharacterIcon from '../img/DownloadCharacter.png';
import ImportIcon from '../img/import.png';
import SaveSpaceshipIcon from '../img/SaveSpaceship.png'
import TrashIcon from '../img/trash-var-solid.png';
import PopOutIcon from '../img/PopOut.png';

/* I/O */
var FileSaver = require('file-saver');

class Ship extends React.Component {


  constructor(props){
     super(props);
     this.state = {
        intervalIsSet: false,
        idToDelete: null,
        idToUpdate: null,
        objectToUpdate: null,
        loadable_state: {
            encounter_name: '',
            showSaveEncounter: false,
            showLoadEncounter: false,
            showCreateShip: false,
            showCreateCharacter: false,
            showDamages: false,
            alerts:{
                'cantAddPlayerWithoutShip': false,
                'cantDownloadCharacterIfNoneExist': false,
                'cantDownloadShipIfNoneExist': false
            },
            showModal:{
                'captain': false,
                'engineer': false,
                'pilot': false,
                'gunner': false,
                'science_officer': false
            },
            data:[[
                    {'axis':'Forward', 'value': 0},
                    {'axis':'Starboard', 'value': 0},
                    {'axis':'Aft', 'value': 0},
                    {'axis':'Port', 'value': 0}        
                ]],
            active_character: -1,
            active_ship: -1,
            ship_data:[]
        }
     };
  }

  handleShieldChange(event){
    const target = event.target;
    const value = target.value;
    const name = target.name;

    let newState = this.state;

    for(let i=0; i<newState.loadable_state.data[0].length; i++){
        if(newState.loadable_state.data[0][i].axis === name){
            newState.loadable_state.ship_data[this.state.loadable_state.active_ship].shields[name.toLowerCase() + '_shields_cur'] = value;
            newState.loadable_state.data[0][i].value = (value / this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].shields[name.toLowerCase() + '_shields_max']) * 4;
        }
    }

    this.setState(newState);
  }


  cancelCreateShipModal(){
    let newState = this.state;
    newState.loadable_state.showCreateShip = false;

    //Get rid of dummy object you created when you showed modal
    newState.loadable_state.ship_data.pop();

    if(newState.loadable_state.ship_data.length === 0){
        newState.loadable_state.active_ship = -1;
    } else {
        newState.loadable_state.active_ship = newState.loadable_state.ship_data.length - 1;
    }
    

    this.setState(newState);
  }

  showCreateShipModal(){
    let newState = this.state;
    newState.loadable_state.showCreateShip = true;

    //Then create a new dummy object
    let newShip = {
        'ship_id': -1,
        'disposition': 'success',
        'selected': false,
        'damage':{
            'weapons_array': 'N',
            'sensors': 'N',
            'life_support': 'N',
            'engines': 'N',
            'power_core': 'N'
        },
        'exposure':{
            'basic_information': true,
            'defenses':  true,
            'weapons': {
                'wpns_turrets': true,
                'wpns_forward': true,
                'wpns_port': true,
                'wpns_starboard': true,
                'wpns_aft': true
            },
            'special_abilities': {
                'spec_ability_1': true,
                'spec_ability_2': true,
                'spec_ability_3': true,
                'spec_ability_4': true,
                'spec_ability_5': true,
                'spec_ability_6': true
            },
            'load': true,
            'other': true
        },
        'shields':{
            'forward_shields_max': -1,
            'forward_shields_cur': -1,
            'starboard_shields_max': -1,
            'starboard_shields_cur': -1,
            'aft_shields_max': -1,
            'aft_shields_cur': -1,
            'port_shields_max': -1,
            'port_shields_cur': -1
        },
        'systems':{
            'thrusters':'',
            'armor':'',
            'computer':'',
            'sensors':'',
            'defensive_countermeasures':''
        },
        'weapons':{
            'forward_weapons': '',
            'starboard_weapons': '',
            'aft_weapons': '',
            'port_weapons': '',
            'turret_weapons': ''
        },
        'special_abilities':{
            'spec_ability_name_1':'',
            'spec_ability_desc_1':'',
            'spec_ability_name_2':'',
            'spec_ability_desc_2':'',
            'spec_ability_name_3':'',
            'spec_ability_desc_3':'',
            'spec_ability_name_4':'',
            'spec_ability_desc_4':'',
            'spec_ability_name_5':'',
            'spec_ability_desc_5':'',
            'spec_ability_name_6':'',
            'spec_ability_desc_6':''
        },
        'hull':{
            'hp_percentage': -1,
            'hp_max': -1,
            'hp_cur': -1,
            'ac_fill': -1,
            'tl_fill': -1,
            'dt_fill': -1,
            'ct_fill': -1
        },
        'stats':{
            'ship_tier': 1,
            'ship_name': '',
            'classification': '',
            'make_and_model': '',
            'crew_compliment': 0,
            'size': '',
            'speed': -1,
            'maneuverability': '',
            'drift_rating': -1,
            'drift_engine': '',
            'power_core': '',
            'misc_systems': '',
            'expansion_bays': '',
            'modifiers': '',
            'cargo':''
        },
        'crew':[

        ]
    }
    newState.loadable_state.ship_data.push(newShip);
    newState.loadable_state.active_ship = newState.loadable_state.ship_data.length - 1;
    this.setState(newState);
  }

  addShip(e){
    //Just close modal for now
    let newState = this.state;
    newState.loadable_state.showCreateShip = false;
    this.setState(newState);
  }

  handleNewShipChange(some_key, e){
    //Step 1: Fetch current state & get current # of ships
    let newState = this.state;
    let numShips = this.state.loadable_state.ship_data.length;
    //Step 2: Get data from form
    const value = e.target.value;
    const name = e.target.name;
    //Step 3: Assign new data to last/newest ship
    //        presumes 'name' is 'name' that is in state
    newState.loadable_state.ship_data[numShips-1][some_key][name] = value;
    //Step 4: Set new state
    this.setState(newState);
  }
  handleDispositionChange(e){
    let newState = this.state;
    let numShips = this.state.loadable_state.ship_data.length;
    const value = e.target.value;
    const name = e.target.name;
    newState.loadable_state.ship_data[numShips-1].disposition = value;
    this.setState(newState);
  }
  handleIdChange(e){
    let newState = this.state;
    let numShips = this.state.loadable_state.ship_data.length;
    const value = e.target.value;
    const name = e.target.name;
    newState.loadable_state.ship_data[numShips-1].ship_id = value;
    this.setState(newState);
  }
  handleShipHPChange(e){
    let newState = this.state;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].hull.hp_cur = e.target.value;
    let hp_cur = newState.loadable_state.ship_data[newState.loadable_state.active_ship].hull.hp_cur;
    let hp_max = newState.loadable_state.ship_data[newState.loadable_state.active_ship].hull.hp_max;
    let hp_percentage = (hp_cur / hp_max) * 100;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].hull.hp_percentage = hp_percentage;
    this.setState(newState);
  }


  handleShipSelectClick(ship_id, e){
    let newState = this.state;
    for(let i=0; i<newState.loadable_state.ship_data.length; i++){
        if(newState.loadable_state.ship_data[i].ship_id === ship_id){
            newState.loadable_state.active_ship = i;
            break;
        }
    }

    //Forward
    newState.loadable_state.data[0][0].value = 
        (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['forward_shields_cur'] 
            / 
        newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['forward_shields_max']) * 4;
    //Starboard
    newState.loadable_state.data[0][1].value = 
        (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['starboard_shields_cur'] 
            / 
        newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['starboard_shields_max']) * 4;
    //Aft
    newState.loadable_state.data[0][2].value = 
        (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['aft_shields_cur'] 
            / 
        newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['aft_shields_max']) * 4;
    //Port
    newState.loadable_state.data[0][3].value = 
        (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['port_shields_cur'] 
            / 
        newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['port_shields_max']) * 4;
    
    this.setState(newState);
  }
  handleShipDeleteClick(ship_id, e){
    let newState = this.state;
    for(let i=0; i<newState.loadable_state.ship_data.length; i++){
        if(newState.loadable_state.ship_data[i].ship_id === ship_id){
            newState.loadable_state.ship_data.splice(i,1)
            break;
        }
    }
    
    if(newState.loadable_state.ship_data.length !== 0){
        newState.loadable_state.active_ship = newState.loadable_state.ship_data.length - 1;
        //Forward
        newState.loadable_state.data[0][0].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['forward_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['forward_shields_max']) * 4;
        //Starboard
        newState.loadable_state.data[0][1].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['starboard_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['starboard_shields_max']) * 4;
        //Aft
        newState.loadable_state.data[0][2].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['aft_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['aft_shields_max']) * 4;
        //Port
        newState.loadable_state.data[0][3].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['port_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['port_shields_max']) * 4;
    } else {
        newState.loadable_state.data[0][0].value = 0
        newState.loadable_state.data[0][1].value = 0
        newState.loadable_state.data[0][2].value = 0
        newState.loadable_state.data[0][3].value = 0

        newState.loadable_state.active_ship = -1;
    }

    this.setState(newState);
  }

  downloadShipData(){
    //File you're creating should always be newest (see the way data is pushed to sate when modal is shown)
    let temp = this.state.loadable_state.ship_data[this.state.loadable_state.ship_data.length-1];
    let dataBlobToDownload = this.state.loadable_state.ship_data[this.state.loadable_state.ship_data.length-1];
    let shipName = this.state.loadable_state.ship_data[this.state.loadable_state.ship_data.length-1].stats.ship_name; 
    let fileName = shipName.replace(/\s+/g, '_'); //replace spaces in ship name with underscores

    let file = new File([JSON.stringify(dataBlobToDownload)], fileName + ".json", {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(file);
  }
  downloadShipState(){

    if(typeof this.state.loadable_state.ship_data[this.state.loadable_state.active_ship] !== 'undefined') {
        //File you're creating should always be newest (see the way data is pushed to sate when modal is shown)
        let dataBlobToDownload = this.state.loadable_state.ship_data[this.state.loadable_state.active_ship];
        let shipName = this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.ship_name; 
        let fileName = shipName.replace(/\s+/g, '_'); //replace spaces in ship name with underscores

        let currentTime = new Date();
        let year = currentTime.getFullYear();
        let month = currentTime.getMonth();
        let day = currentTime.getDate();

        let file = new File([JSON.stringify(dataBlobToDownload)], fileName + "_" + year + "_" + month + "_" + day + ".json", {type: "text/plain;charset=utf-8"});
        FileSaver.saveAs(file);
    } else {
        let newState = this.state;
        newState.loadable_state.alerts.cantDownloadShipIfNoneExist = true;
        this.setState(newState);
    }
  }
  downloadEncounter(){
    //File you're creating should always be newest (see the way data is pushed to sate when modal is shown)
    let dataBlobToDownload = this.state;
    dataBlobToDownload.showSaveEncounter = false;
    let encounterName = this.state.loadable_state.encounter_name; 
    let fileName = encounterName.replace(/\s+/g, '_'); //replace spaces in ship name with underscores

    let currentTime = new Date();
    let year = currentTime.getFullYear();
    let month = currentTime.getMonth();
    let day = currentTime.getDate();

    let file = new File([JSON.stringify(dataBlobToDownload)], fileName + "_" + year + "_" + month + "_" + day + ".json", {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(file);

    let newState = this.state;
    newState.loadable_state.showSaveEncounter = false;
    this.setState(newState);
  }



  importShipData(e){
    let reader = new FileReader();
    let importedData;
    let newState = this.state;
    reader.onload = function(e) {
        importedData = reader.result;
        newState.loadable_state.ship_data[newState.loadable_state.ship_data.length-1] = JSON.parse(importedData);
        
        //Forward
        newState.loadable_state.data[0][0].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['forward_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['forward_shields_max']) * 4;
        //Starboard
        newState.loadable_state.data[0][1].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['starboard_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['starboard_shields_max']) * 4;
        //Aft
        newState.loadable_state.data[0][2].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['aft_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['aft_shields_max']) * 4;
        //Port
        newState.loadable_state.data[0][3].value = 
            (newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['port_shields_cur'] 
                / 
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].shields['port_shields_max']) * 4;
      
        newState.loadable_state.showCreateShip = false;
        this.setState(newState);
    }.bind(this);
    reader.readAsText(e.target.files[0]);
  }

  importEncounter(e){
    let reader = new FileReader();
    let importedData;
    let newState = this.state;
    reader.onload = function(e) {
        importedData = reader.result;
        newState = JSON.parse(importedData);
        console.log(newState);
        newState.loadable_state.showSaveEncounter = false;
        newState.loadable_state.showLoadEncounter = false;
        console.log(newState);
        this.setState(newState);
    }.bind(this);
    reader.readAsText(e.target.files[0]);
  }

  cancelSaveEncounterModal(){
    //Just close modal for now
    let newState = this.state;
    newState.loadable_state.showSaveEncounter = false;
    this.setState(newState);
  }
  showSaveEncounterModal(){
    let newState = this.state;
    newState.loadable_state.showSaveEncounter = true;
    this.setState(newState);
  }

  handleEncounterNameChange(e){
    let newState = this.state;
    newState.loadable_state.encounter_name = e.target.value;
    this.setState(newState);
  }

  cancelLoadEncounterModal(){
    let newState = this.state;
    newState.loadable_state.showLoadEncounter = false;
    this.setState(newState);     
  }
  
  showLoadEncounterModal(){
    let newState = this.state;
    newState.loadable_state.showLoadEncounter = true;
    this.setState(newState);
  }

  showCreateCharacterModal(){
    let newState = this.state;
    newState.loadable_state.showCreateCharacter = true;

    //Then create a new dummy object
    let newCharacter = {
        'characterId':'',
        'characterName':'',
        'playerName':'',
        'baseAttack':'',
        'ship_role':'captain',
        'abilities':{
            'strAbility_mod':0,
            'dexAbility_mod':0,
            'conAbility_mod':0,
            'intAbility_mod':0,
            'wisAbility_mod':0,
            'chaAbility_mod':0
        },
        'skills':{
            'bluff_ranks':0,
            'bluff_class':0,
            'bluff_ability':0,
            'bluff_misc':0,
            'computers_ranks':0,
            'computers_class':0,
            'computers_ability':0,
            'computers_misc':0,
            'diplomacy_ranks':0,
            'diplomacy_class':0,
            'diplomacy_ability':0,
            'diplomacy_misc':0,
            'engineering_ranks':0,
            'engineering_class':0,
            'engineering_ability':0,
            'engineering_misc':0,
            'intimidate_ranks':0,
            'intimidate_class':0,
            'intimidate_ability':0,
            'intimidate_misc':0,
            'lifeScience_ranks':0,
            'lifeScience_class':0,
            'lifeScience_ability':0,
            'lifeScience_misc':0,
            'physicalScience_ranks':0,
            'physicalScience_class':0,
            'physicalScience_ability':0,
            'physicalScience_misc':0,
            'piloting_ranks':0,
            'piloting_class':0,
            'piloting_ability':0,
            'piloting_misc':0,
        }

    }
    if(newState.loadable_state.ship_data[newState.loadable_state.active_ship]) {
        newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.push(newCharacter);
        newState.loadable_state.active_character = newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.length - 1;
        this.setState(newState);
    } else {
        newState.loadable_state.showCreateCharacter = false;
        newState.loadable_state.alerts.cantAddPlayerWithoutShip = true;
        this.setState(newState);
    }
  }






  cancelCreateCharacterModal(){
    let newState = this.state;
    newState.loadable_state.showCreateCharacter = false;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.pop();

    if(newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.length === 0){
        newState.loadable_state.active_character = -1;
    } else {
        newState.loadable_state.active_character = newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.length - 1;
    }
    

    this.setState(newState);
  }

  addCharacter(){
      let newState = this.state;
      newState.loadable_state.showCreateCharacter = false;
      this.setState(newState);
  }

  handleCrewChange(someProperty, e){
    let newState = this.state;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew[newState.loadable_state.active_character][someProperty][e.target.name] = e.target.value;
    this.setState(newState);
  }

  handleCrewDetailChange(e){
    let newState = this.state;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew[newState.loadable_state.active_character][e.target.name] = e.target.value;
    this.setState(newState);
  }
  handleCrewRoleChange(character, e){
    let newState = this.state;
    for(let i=0; i<newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.length; i++){
        if(newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew[i].characterId === character.characterId){
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew[i].ship_role = e.target.value;
            break;
        }
    }
    this.setState(newState);
  }

  handleCharacterDeleteClick(character, e){
    let newState = this.state;
    for(let i=0; i<newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.length; i++){
        if(newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew[i].characterId === character.characterId){
            newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.splice(i,1)
            break;
        }
    }
    this.setState(newState);
  }

  downloadCharacter(){
    //File you're creating should always be newest (see the way data is pushed to sate when modal is shown)
    let temp = this.state.loadable_state.ship_data[this.state.loadable_state.ship_data.length-1];

    if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]){
        let dataBlobToDownload = this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].crew[this.state.loadable_state.active_character];
        let characterName = this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].crew[this.state.loadable_state.active_character].characterName; 
        let fileName = characterName.replace(/\s+/g, '_'); //replace spaces in ship name with underscores
        let file = new File([JSON.stringify(dataBlobToDownload)], fileName + ".json", {type: "text/plain;charset=utf-8"});
        FileSaver.saveAs(file);
    } else {
        let newState = this.state;
        newState.loadable_state.alerts.cantDownloadCharacterIfNoneExist = true;
        this.setState(newState);
    }


  }
  
  importCharacterData(e){
    let reader = new FileReader();
    let importedData;
    let newState = this.state;
    reader.onload = function(e) {
        importedData = reader.result;
        newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew[newState.loadable_state.active_character] = JSON.parse(importedData);
      
        newState.loadable_state.showCreateCharacter = false;
        this.setState(newState);
    }.bind(this);
    reader.readAsText(e.target.files[0]);
  }

  handleExposureCheck(e){
    let newState = this.state;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].exposure[e.target.name] = e.target.checked;
    this.setState(newState);
  }

  handleComplexExposureCheck(secondary, e){
    let newState = this.state;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].exposure[secondary][e.target.name] = e.target.checked;
    this.setState(newState);
  }

  handleCharacterClick(characterId, e){
    let newState = this.state;
    for(let i=0; i<newState.loadable_state.ship_data[newState.loadable_state.active_ship].crew.length; i++){
        if(newState.loadable_state.ship_data[this.state.loadable_state.active_ship].crew[i].characterId === characterId){
            newState.loadable_state.active_character = i;
            break;
        }
    }


    newState.loadable_state.showModal[newState.loadable_state.ship_data[this.state.loadable_state.active_ship].crew[newState.loadable_state.active_character].ship_role] = true;
    this.setState(newState);
  }

  handleCharacterClose(e){
    let newState = this.state;
    newState.loadable_state.showModal[e.target.name] = false;
    this.setState(newState);
  }

  handleShipRoleClose(role){
    let newState = this.state;
    newState.loadable_state.showModal[role] = false;
    this.setState(newState);  
  }

  handleDamageChange(e){
    let newState = this.state;
    newState.loadable_state.ship_data[newState.loadable_state.active_ship].damage[e.target.name] = e.target.value;
    this.setState(newState); 
  }

  handleDamageReportClose(){
    let newState = this.state;
    newState.loadable_state.showDamages= false;
    this.setState(newState);
  }

  showDamageReport(){
    let newState = this.state;
    newState.loadable_state.showDamages= true;
    this.setState(newState);
  }

  handleAlertClose(alert_type){
      let newState = this.state;
      switch(alert_type){
          case 'Alert_CantCreatePilotWithoutShip':
            newState.loadable_state.alerts.cantAddPlayerWithoutShip = false;
            break;
          case 'Alert_CantDownloadCharacterIfNoneExist':
            newState.loadable_state.alerts.cantDownloadCharacterIfNoneExist = false;
            break;
          case 'Alert_CantDownloadShipIfNoneExist':
            newState.loadable_state.alerts.cantDownloadShipIfNoneExist = false;
          default:
              console.log('alert type not recognized.');
      }
      this.setState(newState);
  }



  /**
   * LIFECYCLE/DB METHODS
   */
  componentDidMount() {
    this.getDataFromDb();
    //console.log(this.state.loadable_state);
    // We don't need polling, otherwise changes will override user actions
    /*
    if (!this.state.intervalIsSet) {
      let interval = setInterval(this.getDataFromDb, 1000);
      this.setState({ intervalIsSet: interval });
    }
    */
  }

  componentWillUnmount() {
    // Polling isn't needed, otherwise we would need to clear the interval like below
    /*
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null });
    }
    */
  }

  getDataFromDb = () => {
    fetch("http://52.32.94.231:3001/api/ship_encounter/getData")
      .then(data => data.json())
      .then(res => this.setState({ loadable_state: res.data[0].state }));
   /*
   fetch("http://localhost:3001/api/ship_encounter/getData")
    .then(data => data.json())
    .then(res => {
        console.log(res.data[0].state);
    }, err => {
        console.log(err);
    });
    */
  };

  putDataToDB = stateToSave => {
    axios.post("http://52.32.94.231:3001/api/ship_encounter/putData", { 
        state: stateToSave
    });
  };

  deleteFromDB = idTodelete => {
    let objIdToDelete = null;
    this.state.data.forEach(dat => {
      if (dat.id === idTodelete) {
        objIdToDelete = dat._id;
      }
    });

    axios.delete("http://52.32.94.231:3001/api/ship_encounter/deleteData", {
      data: {
        id: objIdToDelete
      }
    });
  };

  updateDB = (updateToApply) => {
    //first, fetch all documents. There should always be only one.
    //.. TODO
    /*
    fetch("http://localhost:3001/api/ship_encounter/getData")
        .then(data => data.json())
        .then(res => {
        console.log(res.data[0]._id);
        }, err => {
            console.log(err);
        });
    */
   //console.log(updateToApply);
   
    fetch("http://52.32.94.231:3001/api/ship_encounter/getData")
        .then(data => data.json())
        .then(res => res.data[0]._id)
        .then(_id => axios.post("http://52.32.94.231:3001/api/ship_encounter/updateData", {
            id: _id,
            update: { state: updateToApply }
        }))
    
  };






  render() {
    let active_ship;
    if(typeof this.state.loadable_state !== 'undefined'){
        active_ship = this.state.loadable_state.active_ship;
    }
    let ShipStatsDisplay;
    let ExposureList;
    let ExpansionBaysAndSystems;
    let WeaponsList;
    let WeaponsTurret;
    let WeaponsForward;
    let WeaponsPort;
    let WeaponsStarboard;
    let WeaponsAft;
    let SpecialAbilities;
    let DamageReport;

    let BasicInformation = {
        'name':'',
        'tier':'',
        'classification':'',
        'makeAndModel':'',
        'size':'',
        'speed':'',
        'maneuverability':'',
        'crew_compliment':0
    }
    let Defenses = {
        'shieldForm':'',
        'shieldDisplay':'',
        'hull':'',
        'pcuCore':'',
        'damages':'',
        'armor':'',
        'defensive_countermeasures':''
    };
    let Systems = {
        'thrusters':'',
        'computer':'',
        'sensors':''
    };
    let OtherInformation = {
        'driftEngine':'',
        'driftRating':'',
        'misc_systems':''
    }
    let Load = {
        'expansionBays':'',
        'cargo':''
    }
   

    let CharacterList = <div/>
    if(typeof this.state.loadable_state !== 'undefined' && active_ship !== -1){
        SpecialAbilities = <SpecialAbilitiesList ship_active={this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]}/>
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.defenses){

            DamageReport = <DamageReportModal showModal={this.state.loadable_state.showDamages} onCloseRequest={() => this.handleDamageReportClose()} ship_active={this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]}/>;



            Defenses['shieldForm'] = <ShieldForm ship_active={this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]} onShieldChange={(e) => this.handleShieldChange(e)} />;
            
            Defenses['damages'] = <DamageForm onShowDamageReport={() => this.showDamageReport()} ship_active={this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]} onDamageChange={(e) => this.handleDamageChange(e)}/>

            Defenses['pcuCore'] = <ListGroup.Item><strong>Power Core: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.power_core}</ListGroup.Item>;
            Defenses['hull'] =  <HullForm onShipHPChange={(e) => this.handleShipHPChange(e)} ship_active={this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]}/>;
                                
        
            Defenses['shieldDisplay'] = <ShipShields data={this.state.loadable_state.data}/>;
            Defenses['armor'] = <ListGroup.Item><strong>Armor: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].systems.armor} </ListGroup.Item>;
            Defenses['defensive_countermeasures'] = <ListGroup.Item><strong>Def. Counter: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].systems.defensive_countermeasures} </ListGroup.Item>;
        }
        /**
         *             'forward_weapons': '',
            'starboard_weapons': '',
            'aft_weapons': '',
            'port_weapons': '',
            'turret_weapons': ''
         */
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.weapons['wpns_turrets']){
            WeaponsTurret = <ListGroup.Item><strong>Turrets: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].weapons.turret_weapons} </ListGroup.Item>
        }
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.weapons['wpns_forward']){
            WeaponsForward = <ListGroup.Item><strong>Forward: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].weapons.forward_weapons} </ListGroup.Item>
        }
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.weapons['wpns_port']){
            WeaponsPort = <ListGroup.Item><strong>Port: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].weapons.port_weapons} </ListGroup.Item>
        }
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.weapons['wpns_starboard']){
            WeaponsStarboard = <ListGroup.Item><strong>Starboard: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].weapons.starboard_weapons} </ListGroup.Item>
        }         
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.weapons['wpns_aft']){
            WeaponsAft = <ListGroup.Item><strong>Aft: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].weapons.aft_weapons} </ListGroup.Item>
        }




        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.basic_information){
            BasicInformation['name'] = <ListGroup.Item><strong>Name: </strong><br/>{this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.ship_name} </ListGroup.Item>;
            BasicInformation['classification'] = <ListGroup.Item><strong>Class: </strong><br/>{this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.classification} </ListGroup.Item>;
            BasicInformation['tier'] = <ListGroup.Item><strong>Tier: </strong>{this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.ship_tier} </ListGroup.Item>;
            BasicInformation['makeAndModel'] = <ListGroup.Item><strong>Make and Model: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.make_and_model} </ListGroup.Item>;
            BasicInformation['size'] = <ListGroup.Item><strong>Size: </strong>  {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.size} </ListGroup.Item>;
            BasicInformation['speed'] = <ListGroup.Item><strong>Speed: </strong> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.speed} </ListGroup.Item>;
            BasicInformation['maneuverability'] = <ListGroup.Item><strong>Maneuverability: </strong> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.maneuverability} </ListGroup.Item>;
            BasicInformation['crew_compliment'] = <ListGroup.Item><strong>Crew: </strong> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.crew_compliment} </ListGroup.Item>;
        }
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.other){
            OtherInformation['driftRating'] = <ListGroup.Item><strong>Drift Rating: </strong> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.drift_rating} </ListGroup.Item>;
            OtherInformation['driftEngine'] = <ListGroup.Item><strong>Drift Engine: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.drift_engine} </ListGroup.Item>;
            OtherInformation['misc_systems'] = <ListGroup.Item><strong>Misc Systems: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.misc_systems} </ListGroup.Item>;
            Systems['computer'] = <ListGroup.Item><strong>Computer: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].systems.computer} </ListGroup.Item>;
            Systems['thrusters'] = <ListGroup.Item><strong>Thrusters: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].systems.thrusters} </ListGroup.Item>;
            Systems['sensors'] = <ListGroup.Item><strong>Thrusters: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].systems.sensors} </ListGroup.Item>;
        }
        if(this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].exposure.load){
            Load['expansionBays'] = <ListGroup.Item><strong>Expansion Bays: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.expansion_bays} </ListGroup.Item>;
            Load['cargo'] = <ListGroup.Item><strong>Cargo: </strong><br/> {this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].stats.cargo} </ListGroup.Item>;
        }
        
        ExposureList = <Exposure ship_active={this.state.loadable_state.ship_data[this.state.loadable_state.active_ship]} 
                                 onExposureCheck={(e) => this.handleExposureCheck(e)} 
                                 onComplexExposureCheck={(secondary, e) => this.handleComplexExposureCheck(secondary, e)}/>
                        
        ShipStatsDisplay = <ListGroup>
                                {BasicInformation['name']}
                                {BasicInformation['tier']}
                                {BasicInformation['makeAndModel']}
                                {BasicInformation['crew_compliment']}
                                {BasicInformation['size']}
                                {BasicInformation['speed']}
                                {BasicInformation['maneuverability']}
                                {OtherInformation['driftRating']}
                                {OtherInformation['driftEngine']}
                                {Defenses['pcuCore']}
                                {Defenses['armor']}
                                {Defenses['defensive_countermeasures']}
                                {Systems['thrusters']}
                                {Systems['sensors']}
                                {Systems['computer']}
                                <ListGroup.Item>
                                    {ExposureList}
                                </ListGroup.Item>
                            </ListGroup>;

        WeaponsList = <ListGroup>
                        {WeaponsTurret}
                        {WeaponsForward}
                        {WeaponsPort}
                        {WeaponsStarboard}
                        {WeaponsAft}
                      </ListGroup>;
                      
        

        CharacterList = this.state.loadable_state.ship_data[this.state.loadable_state.active_ship].crew.map((character) =>
            <SingleCharacter key={character.characterId}
                character={character} 
                onCharacterClick={(charId, e) => this.handleCharacterClick(charId, e)}
                onCharacterDelete={(char, e) => this.handleCharacterDeleteClick(char, e)}
                onCrewRoleChange={(char, e) => this.handleCrewRoleChange(char, e)}/>
        );
        ExpansionBaysAndSystems = <ListGroup>
                                        {Load['expansionBays']}
                                        {Load['cargo']}
                                        {OtherInformation['systems']}
                                  </ListGroup>;
        
    }

    var ShipList;
    if(typeof this.state.loadable_state !== 'undefined'){
        ShipList = this.state.loadable_state.ship_data.map((ship) =><SingleShip key={ship.ship_id}
            ship={ship} 
            onShipSelect={(e) => this.handleShipSelectClick(ship.ship_id, e)} 
            onShipDelete={(e) => this.handleShipDeleteClick(ship.ship_id, e)}/>
        );
    } else {
        ShipList = <div/>
    }

    var alert_cantCreatePilotWithoutShip;
    var alert_CantDownloadCharacterIfNoneExist;
    var alert_CantDownloadShipIfNoneExist;

    var ship_create_modal;
    var encounter_save_modal;
    var encounter_load_modal;
    var character_modal;
    var captain_modal;
    var pilot_modal;
    var engineer_modal;
    var gunner_modal;
    var scienceofficer_modal;

    if(typeof this.state.loadable_state !== 'undefined'){
        alert_cantCreatePilotWithoutShip = <Alert_CantCreatePilotWithoutShip showAlert={this.state.loadable_state.alerts.cantAddPlayerWithoutShip} onCloseRequest={() => this.handleAlertClose('Alert_CantCreatePilotWithoutShip')}/>
        alert_CantDownloadCharacterIfNoneExist = <Alert_CantDownloadCharacterIfNoneExist showAlert={this.state.loadable_state.alerts.cantDownloadCharacterIfNoneExist} onCloseRequest={() => this.handleAlertClose('Alert_CantDownloadCharacterIfNoneExist')}/>
        alert_CantDownloadShipIfNoneExist = <Alert_CantDownloadShipIfNoneExist showAlert={this.state.loadable_state.alerts.cantDownloadShipIfNoneExist} onCloseRequest={() => this.handleAlertClose('Alert_CantDownloadShipIfNoneExist')}/>

        {/** SHIP CREATE MODULE */}
        ship_create_modal = <ShipCreateModal showModal={this.state.loadable_state.showCreateShip}
                                onCancelRequest={() => this.cancelCreateShipModal()}
                                onAddShip={(e) => this.addShip(e)}
                                onDownload={() => this.downloadShipData()}
                                onNewShipChange={(secondary, e) => this.handleNewShipChange(secondary, e)}
                                exposure={ExposureList}
                                onImport={(e) => this.importShipData(e)}
                                onIdChange={(e) => this.handleIdChange(e)}
                                onDispositionChange={(e) => this.handleDispositionChange(e)}/>

        {/** ENCOUNTER SAVE MODAL */}
        encounter_save_modal = <EncounterSaveModal showModal={this.state.loadable_state.showSaveEncounter}
                                    onDownloadEncounter={() => this.downloadEncounter()}
                                    onEncounterNameChange={(e) => this.handleEncounterNameChange(e)}
                                    onCancelRequest={() => this.cancelSaveEncounterModal()}/>

        {/** ENCOUNTER LOAD MODAL */}
        encounter_load_modal = <EncounterLoadModal showModal={this.state.loadable_state.showLoadEncounter} 
                                    onCancelRequest={() => this.cancelLoadEncounterModal()} 
                                    onImportEncounter={(e) => this.importEncounter(e)}/>

        {/** Character Creation Form */}
        character_modal = <CharacterModal showModal={this.state.loadable_state.showCreateCharacter}
                                onCrewChange={(secondary, e) => this.handleCrewChange(secondary, e)}
                                onCrewDetailChange={(e) => this.handleCrewDetailChange(e)}
                                onImportCharacterData={(e) => this.importCharacterData(e)}
                                onDownloadRequest={() => this.downloadCharacter()}
                                onAddCharacterRequest={() => this.addCharacter()}
                                onCancelRequest={() => this.cancelCreateCharacterModal()}
                                />

        {/** CAPTAIN */}
        captain_modal = <CaptainModal showModal={this.state.loadable_state.showModal.captain} onCloseRequest={() => this.handleShipRoleClose('captain')}/>

        {/** PILOT */}
        pilot_modal = <PilotModal showModal={this.state.loadable_state.showModal.pilot} onCloseRequest={() => this.handleShipRoleClose('pilot')}/>

        {/** ENGINEER */}
        engineer_modal = <EngineerModal showModal={this.state.loadable_state.showModal.engineer} onCloseRequest={() => this.handleShipRoleClose('engineer')}/>

        {/** GUNNER */}
        gunner_modal = <GunnerModal showModal={this.state.loadable_state.showModal.gunner} onCloseRequest={() => this.handleShipRoleClose('gunner')}/>  

        {/** SCIENCE OFFICER */}
        scienceofficer_modal = <ScienceOfficerModal showModal={this.state.loadable_state.showModal.science_officer} onCloseRequest={() => this.handleShipRoleClose('science_officer')}/>  
    } else {
        alert_cantCreatePilotWithoutShip = <div/>
        alert_CantDownloadCharacterIfNoneExist = <div/>
        alert_CantDownloadShipIfNoneExist = <div/>
    }

    
    return (
      <div>
      {alert_cantCreatePilotWithoutShip}
      {alert_CantDownloadCharacterIfNoneExist}
      {alert_CantDownloadShipIfNoneExist}
      
      <Row>
          <Col md={2}>
              <ButtonGroup>
                <OverlayTrigger key='grab-changes-db-key' placement='right'
                    overlay={
                        <Tootip>
                            Grabs changes from the database
                        </Tootip>
                    }
                >
                    <Button variant="secondary" size="sm" onClick={() => this.getDataFromDb()}>Fetch Changes</Button>
                </OverlayTrigger>
                <OverlayTrigger key='push-changes-db-key' placement='right'
                    overlay={
                        <Tootip>
                            Push changes to database
                        </Tootip>
                    }
                >
                    <Button variant="warning" size="sm" onClick={() => this.updateDB(this.state.loadable_state)}>Push Changes</Button>
                </OverlayTrigger>
              </ButtonGroup>
          </Col>
      </Row>
      <Row>
        <Col md={2}>
            <h2 className="word_fanciness">Ships</h2>
            {ShipList}
            <hr/>
            <ButtonGroup>
                <OverlayTrigger key='create-ship-key' placement='right'
                    overlay={
                        <Tootip>
                            Create Ship
                        </Tootip>
                    }
                >
                    <Button variant="light" size="sm" onClick={() => this.showCreateShipModal()}><Image src={CreateSpaceshipIcon} className="icon_size" thumbnail /></Button>
                </OverlayTrigger>
                <OverlayTrigger key='save-ship-key' placement='right'
                    overlay={
                        <Tootip>
                            Save Ship (download)
                        </Tootip>
                    }
                >
                    <Button variant="light" size="sm" onClick={() => this.downloadShipState()} ><Image src={SaveSpaceshipIcon} className="icon_size" thumbnail /></Button>
                </OverlayTrigger>
                <OverlayTrigger key='save-encounter-key' placement='right'
                    overlay={
                        <Tootip>
                            Save Encounter (download)
                        </Tootip>
                    }
                >
                    <Button variant="light" size="sm" onClick={() => this.showSaveEncounterModal()}><Image src={DownloadIcon} className="icon_size" thumbnail /></Button>
                </OverlayTrigger>
                <OverlayTrigger key='load-encounter-key' placement='right'
                    overlay={
                        <Tootip>
                            Load Encounter (select json file)
                        </Tootip>
                    }
                >
                    <Button variant="light" size="sm" onClick={() => this.showLoadEncounterModal()}><Image src={ImportIcon} className="icon_size" thumbnail /></Button>
                </OverlayTrigger>
            </ButtonGroup>
            <hr/>
            <h2 className="word_fanciness">Characters</h2>
            {CharacterList}
            <hr/>
            <ButtonGroup>
                <OverlayTrigger key='create-character-key' placement='right'
                    overlay={
                        <Tootip>
                            Create Character
                        </Tootip>
                    }
                >
                    <Button variant="light" size="sm" onClick={() => this.showCreateCharacterModal()}><Image src={AddCharacterIcon} className="icon_size" thumbnail /></Button>
                </OverlayTrigger>
                <OverlayTrigger key='save-character-key' placement='right'
                    overlay={
                        <Tootip>
                            Save Character (download)
                        </Tootip>
                    }
                >
                    <Button variant="light" size="sm" onClick={() => this.downloadCharacter()}><Image src={DowloadCharacterIcon} className="icon_size" thumbnail /></Button>
                </OverlayTrigger>
                
            </ButtonGroup>
            <hr/>



        </Col>
        <Col md={1}>
            {Defenses['shieldForm']}
            {Defenses['damages']}
        </Col>
        <Col md={7}>
            {Defenses['shieldDisplay']}
            {Defenses['hull']}
            <h6>Misc.</h6>
            {ExpansionBaysAndSystems}
            <h6>Weapons</h6>
            {WeaponsList}
            <h6>Special Abilities</h6>
            {SpecialAbilities}
        </Col>
        <Col md={2}>
            <h2 className="word_fanciness">Stats</h2>
            {ShipStatsDisplay}
        </Col>
      </Row>
      

      {/** SHIP CREATE MODULE */}
      {ship_create_modal}

      {/** ENCOUNTER SAVE MODAL */}
      {encounter_save_modal}

      {/** ENCOUNTER LOAD MODAL */}
      {encounter_load_modal}

      {/** Character Creation Form */}
      {character_modal}

      {/** CAPTAIN */}
      {captain_modal}

      {/** PILOT */}
      {pilot_modal}
      
      {/** ENGINEER */}
      {engineer_modal}

      {/** GUNNER */}
      {gunner_modal} 

      {/** SCIENCE OFFICER */}
      {scienceofficer_modal}
      
      {/** Damages */}
      {DamageReport}
      



    </div>

    );
  }
}

export default Ship;